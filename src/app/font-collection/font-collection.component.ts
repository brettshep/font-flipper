import { Component, OnInit } from "@angular/core";
import { Store } from "../store";
import { CatID, ICatGroup, IFontListGroup } from "../interfaces";
import { first } from "rxjs/operators";
import { FontList } from "./font-list/font-list";
import { Box } from "../canvas-editor/canvas/box";

@Component({
  selector: "font-collection",
  template: `
  <div class="wrapper">
    <div class="center">
      <font-list
      *ngIf="fontListGroup[0]"
      [fontList]="fontListGroup[0]"
      
      ></font-list>
      <font-list
      *ngIf="fontListGroup[1]"
      [fontList]="fontListGroup[1]"

      ></font-list>
      <font-list
      *ngIf="fontListGroup[2]"
      [fontList]="fontListGroup[2]"

      ></font-list>
    </div>
  </div>
  <bottom-controls 
  class="bottom"
  (boxOut)="toggleBox()"
  (abcOut)="toggleABC()"
  (typeOut)="toggleType($event)"
  ></bottom-controls>
  `,
  styleUrls: ["./font-collection.component.sass"]
})
export class FontCollectionComponent implements OnInit {
  fontListGroup: IFontListGroup = { 0: null, 1: null, 2: null };
  catMap = {
    0: "H1",
    1: "H2",
    2: "Body"
  };
  abc = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  // testData: FontList = new FontList("H1");
  constructor(private store: Store) {
    // this.testData.boxText = "Oh how the wind blows.";
    // this.testData.fonts = [
    //   { name: "Ubuntu Sans", link: this.getFontLink("Ubuntu") },
    //   { name: "Ubuntu", link: this.getFontLink("Ubuntu") },
    //   { name: "Ubuntu Hero", link: this.getFontLink("Ubuntu") }
    // ];
    // this.fontListGroup[0] = { ...this.testData };
    // this.testData.category = "H2";
    // this.fontListGroup[1] = { ...this.testData };
  }

  ngOnInit() {
    //cats used
    this.store
      .select<CatID[]>("catsUsed")
      .pipe(first())
      .subscribe(val => {
        if (val) {
          val.forEach(id => {
            this.fontListGroup[id] = new FontList(this.catMap[id]);
          });
        }
      });

    //get font names per cat
    this.store
      .select<ICatGroup>("catSaveGroup")
      .pipe(first())
      .subscribe(val => {
        if (val) {
          for (const catKey in val) {
            if (val[catKey]) {
              this.fontListGroup[catKey].fonts = val[catKey].liked.fonts.map(
                index => {
                  let name = FontData[index].f;
                  return { name, link: this.getFontLink(name) };
                }
              );
            }
          }
        }
      });
    //get boxes text
    this.store
      .select<Box[]>("boxes")
      .pipe(first())
      .subscribe(val => {
        if (val) {
          val.forEach(box => {
            this.fontListGroup[box.boxCat].boxText = box.text;
          });
        }
      });

    this.toggleBox();
  }

  toggleBox() {
    for (const key in this.fontListGroup) {
      const elem: FontList = this.fontListGroup[key];
      if (elem !== null) {
        elem.text = elem.boxText;
      }
    }
  }
  toggleABC() {
    for (const key in this.fontListGroup) {
      const elem: FontList = this.fontListGroup[key];
      if (elem !== null) {
        elem.text = this.abc;
      }
    }
  }
  toggleType(text: string) {
    for (const key in this.fontListGroup) {
      const elem: FontList = this.fontListGroup[key];
      if (elem !== null) {
        elem.text = text;
      }
    }
  }

  getFontLink(name: string): string {
    name = name.replace(/ /g, "+");
    return `https://fonts.google.com/specimen/${name}`;
  }
}
