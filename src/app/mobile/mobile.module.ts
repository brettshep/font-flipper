import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CommonModule } from "@angular/common";
import { MobileComponent } from "./mobile/mobile.component";
import { SharedModule } from "../shared/shared.module";

export const ROUTES: Routes = [
  {
    path: "",
    component: MobileComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), SharedModule],
  declarations: [MobileComponent]
})
export class MobileModule {}
