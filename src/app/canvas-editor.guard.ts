import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Store } from "./store";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: "root"
})
export class CanvasEditorGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select<number>("image").pipe(
      map(val => {
        if (val) {
          return true;
        } else {
          this.router.navigate([`/upload`]);
          return false;
        }
      })
    );
  }
}
