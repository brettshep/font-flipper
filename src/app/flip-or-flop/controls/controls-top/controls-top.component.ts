import { FilterStatus, IFilterStatusGroup } from "./../../../interfaces";
import { ViewChild, ElementRef, ChangeDetectorRef } from "@angular/core";
import { CatID } from "../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-top",
  template: `
    <div class="topPanel">
      <div class="blank"></div>
      <div class="filterCont">
        <button 
          class="box filter" 
          [class.active]="states['SANS_SERIF'] === 1"
          (click)="toggleState('SANS_SERIF')" tooltip="Sans Serif" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
          <img src="/assets/styles/SS.png" alt="style" *ngIf="states['SANS_SERIF'] !== 2;else refresh">
         <ng-template #refresh>
          <i class="fas fa-sync-alt"></i>
         </ng-template>
        </button>
        <button 
          class="box filter"
          [class.active]="states['SERIF']=== 1"
          (click)="toggleState('SERIF')" tooltip="Serif" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
          <img src="/assets/styles/S.png" alt="style" *ngIf="states['SERIF'] !== 2;else refresh">
         <ng-template #refresh>
          <i class="fas fa-sync-alt"></i>
         </ng-template>
        </button>
        <button
         class="box filter"
         [class.active]="states['DISPLAY']=== 1"
         (click)="toggleState('DISPLAY')" tooltip="Display" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
         <img src="/assets/styles/D.png" alt="style" *ngIf="states['DISPLAY'] !== 2;else refresh">
         <ng-template #refresh>
          <i class="fas fa-sync-alt"></i>
         </ng-template>
        </button>
        <button
         class="box filter"
         [class.active]="states['MONOSPACE']=== 1"
         (click)="toggleState('MONOSPACE')" tooltip="Monospace" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
         <img src="/assets/styles/M.png" alt="style" *ngIf="states['MONOSPACE'] !== 2;else refresh">
         <ng-template #refresh>
          <i class="fas fa-sync-alt"></i>
         </ng-template>
        </button>  
        <button
         class="box filter"
         [class.active]="states['HANDWRITING']=== 1"
         (click)="toggleState('HANDWRITING')" tooltip="Handwriting" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
         <img src="/assets/styles/H.png" alt="style" *ngIf="states['HANDWRITING'] !== 2;else refresh">
         <ng-template #refresh>
          <i class="fas fa-sync-alt"></i>
         </ng-template>
        </button>  
      </div>
      <div class="styleSelector" #align>
            <div 
              class="currBoxIcon box active"
              (click)="toggleBoxCatDrop()" >
              {{boxCatString}}
              <i class="fas fa-sort-down"></i>
            </div>
            <div class="boxDropIcons" [ngStyle]="{'height': dropHeight}" >
              <div *ngIf="availCats.includes(0)" class="boxDropIcon" (click)="emitBoxCat(0)">
                H1
              </div>
              <div *ngIf="availCats.includes(1)" class="boxDropIcon" (click)="emitBoxCat(1)">
                H2
              </div>
              <div *ngIf="availCats.includes(2)" class="boxDropIcon" (click)="emitBoxCat(2)">
                Body
              </div>     
            </div>
          </div>
    </div>
  `,
  styleUrls: ["./controls-top.component.sass"]
})
export class ControlsTopComponent {
  @ViewChild("align") align: ElementRef;
  @Output() refresh = new EventEmitter<string>();
  @Output() outBoxCat = new EventEmitter<CatID>();
  states: IFilterStatusGroup;
  boxCatString: string;
  boxCatDropActive: boolean = false;
  listenerBind: any = this.boxDropListener.bind(this);
  catID: CatID;
  @Input()
  set setBoxCat(val) {
    this.catID = val;
    switch (val) {
      case 0:
        this.boxCatString = "H1";
        break;
      case 1:
        this.boxCatString = "H2";
        break;
      case 2:
        this.boxCatString = "Body";
        break;

      default:
        break;
    }
  }

  @Input() availCats: CatID[];

  @Input()
  set catFilter(val) {
    if (val) {
      this.states = val;
    }
  }

  constructor(private cd: ChangeDetectorRef) {}

  get dropHeight() {
    if (this.boxCatDropActive) {
      return this.availCats.length * 48 + "px";
    } else {
      return "0px";
    }
  }

  ngOnDestroy() {
    document.removeEventListener("mousedown", this.listenerBind);
  }

  toggleBoxCatDrop() {
    if (this.catID !== null) {
      this.boxCatDropActive = true;
      document.addEventListener("mousedown", this.listenerBind);
    }
  }

  boxDropListener(e: MouseEvent) {
    const target = e.target;
    const clickedInside = this.align.nativeElement.contains(target);
    if (!clickedInside) {
      document.removeEventListener("mousedown", this.listenerBind);
      this.boxCatDropActive = false;
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    }
  }

  emitBoxCat(e: CatID) {
    this.boxCatDropActive = false;
    this.outBoxCat.emit(e);
  }

  toggleState(state: string) {
    //active
    if (this.states[state] === FilterStatus.active) {
      this.states[state] = FilterStatus.disabled;
      //disabled
    } else if (this.states[state] === FilterStatus.disabled) {
      this.states[state] = FilterStatus.active;
    }
    //empty
    else {
      this.refresh.emit(state);
    }
    // this.filter.emit({ state: this.states[state], styleName: state });
  }
}
