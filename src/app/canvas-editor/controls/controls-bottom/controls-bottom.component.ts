import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
  ElementRef,
  Output,
  EventEmitter
} from "@angular/core";
import { Box } from "../../canvas/box";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-bottom",
  template: `
    <div class="bottomPanel">
      <div class="spacer"></div>
      <div class="textRel">
        <div class="help" *ngIf="!currBox">
          <span class="clickHelp">
            <i class="fas fa-mouse-pointer"></i>
            Click to place text
          </span>
          <span class="dragHelp">
            <img src="/assets/ClickDragIcon.png" alt="clickIcon">
            Click + Drag to place bounded text box
          </span> 
        </div>
        <textarea 
        placeholder="Type in me..." 
        #textbox [(ngModel)]="text" 
        (input)="onInput()" 
        (blur)="shrinkTextBox()" 
        (focus)="focus()"
        [class.active]="currBox"
        ></textarea>
      </div>
      <button class="arrow" (click)="nextPage.emit()" [disabled]="disabled">
        <i class="fas fa-chevron-right"></i>
      </button> 
    </div>
  `,
  styleUrls: ["./controls-bottom.component.sass"]
})
export class ControlsBottomComponent {
  @ViewChild("textbox") textbox: ElementRef;
  @Input() boxCount;
  @Input()
  set setText(text) {
    this.text = text;
  }
  @Input()
  set box(box) {
    this.currBox = box;
    if (!box) {
      this.text = "";
    }
  }
  @Output() outText = new EventEmitter<string>();
  @Output() nextPage = new EventEmitter();
  currBox: Box;
  text: string;

  get disabled() {
    return this.boxCount <= 0;
  }

  ngAfterViewInit() {
    this.resizeTextBox();
  }

  //this requests drawframe due to keypress listener in canvascomp
  onInput() {
    this.resizeTextBox();
    this.outText.emit(this.text);
  }

  resizeTextBox() {
    const elem: HTMLTextAreaElement = this.textbox.nativeElement;
    if (elem.scrollHeight > elem.clientHeight) {
      elem.style.height = elem.scrollHeight + "px";
    } else {
      elem.style.height = 0 + "px";
      elem.style.height = elem.scrollHeight + "px";
    }
  }
  shrinkTextBox() {
    this.textbox.nativeElement.style.height = "54px";
  }

  focus() {
    const elem: HTMLTextAreaElement = this.textbox.nativeElement;
    if (this.text === "My Text...") elem.select();
    this.resizeTextBox();
  }
}
