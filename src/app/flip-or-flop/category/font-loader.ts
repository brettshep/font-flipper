import { IFontFamily } from "../../interfaces";
import { Subject } from "rxjs";

export class FontLoader {
  hasLoaded$ = new Subject<boolean>();

  loadFonts(indices: number[], callback: Function) {
    this.webFontLoad(indices.map(i => this.genSingleWeightString(i)), callback);
  }

  private webFontLoad(fonts: string[], callback: Function) {
    //call load
    WebFont.load({
      google: {
        families: fonts
      },
      active: () => {
        //STUFF
        this.hasLoaded$.next(true);
      },
      //on font active
      fontactive: callback,
      //if font fails
      fontinactive: familyName => {
        console.log("error: ", familyName);
      }
    });
  }

  genSingleWeightString(i: number): string {
    //family
    let family: IFontFamily = { ...FontData[i] };
    //name
    const name = family.f.replace(/ /g, "+");
    //has regular 400 weight
    if (family.v.includes("400")) {
      return `${name}:400`;
    } else {
      let weight = family.v[Math.floor(family.v.length / 2)];
      let desiredWeight = weight.replace(/i/, "");
      if (family.v.includes(desiredWeight)) {
        return `${name}:${desiredWeight}`;
      } else {
        return `${name}:${weight}`;
      }
    }
  }
}
