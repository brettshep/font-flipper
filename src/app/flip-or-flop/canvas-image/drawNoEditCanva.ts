import { CatID } from "./../../interfaces";
import { Box } from "../../canvas-editor/canvas/box";
import { CanvasImageComponent } from "./canvas-image.component";
import { MousePos } from "../../canvas-editor/canvas/canvas.component";

export class DrawNoEditCanvas {
  constructor(private cc: CanvasImageComponent) {}
  boxes: Box[] = [];
  //#region ---------DRAW-----------

  drawImage() {
    this.cc.ctx.drawImage(
      this.cc.img,
      0,
      0,
      this.cc.canvasElem.width,
      this.cc.canvasElem.height
    );
  }

  reqDrawAll() {
    requestAnimationFrame(this.drawAll.bind(this));
  }

  drawAll() {
    //clear
    this.cc.ctx.clearRect(
      0,
      0,
      this.cc.canvasElem.width,
      this.cc.canvasElem.height
    );

    //image
    this.drawImage();

    //boxes
    if (this.boxes) {
      this.boxes.forEach(box => {
        box.draw("rgba(0,0,0,0)");
        //box.draw("blue");
      });
    }
  }

  setBoxFont(family: string, style: string, weight: string, cat: CatID) {
    // //get font height
    // let d = document.createElement("span");
    // d.style.font = `16px ${family}`;
    // d.textContent =
    //   "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    // document.body.appendChild(d);
    // const height = d.offsetHeight;
    // document.body.removeChild(d);
    // console.log(height);
    //set box props
    this.boxes.forEach((box: Box) => {
      if (box.boxCat === cat) {
        // box.baseFontHeight = height;
        box.fontFamily = family;
        box.fontWeight = weight;
        box.fontStyle = style;
      }
    });

    this.reqDrawAll();
    setTimeout(() => {
      this.reqDrawAll();
    }, 200);
  }
  setBoxWeight(name: string, cat: CatID) {}

  //#endregion

  //#region ----------UTILITES------------

  canvasHeightWidth(): { height: number; width: number } {
    return {
      height: this.cc.sizeMap.baseHeight,
      width: this.cc.sizeMap.baseWidth
    };
  }

  createBoxes(boxes: Box[]) {
    if (boxes) {
      boxes.forEach(box => {
        this.boxes.push(
          new Box(
            box.x1,
            box.y1,
            box.x2,
            box.y2,
            this.cc.ctx,
            this.reqDrawAll.bind(this),
            box.boxType,
            this.cc.boxStyles,
            box.boxCat,
            this.canvasHeightWidth.bind(this),
            this.cc.sizeMap,
            { val: 0 },
            box.text
          )
        );
      });
      this.reqDrawAll();
    }
  }

  //#endregion
}
