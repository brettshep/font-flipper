import { LoadingQueue } from "./loading-queue";
import {
  SANS_SERIF,
  MONOSPACE,
  DISPLAY,
  SERIF,
  HANDWRITING
} from "../fontdata";
import { IPipeGroup, IPipe, IFontInfo } from "../../interfaces";
import { FontLoader } from "./font-loader";
import { Category } from "./category";

export class CatFonts {
  fontLoader: FontLoader = new FontLoader();
  queueThresh: number = 5;
  queueAddNum: number = 5;
  loadingQueue: LoadingQueue = new LoadingQueue();
  PG: IPipeGroup = {
    SANS_SERIF: {
      unUsed: [],
      queue: [],
      viewed: []
    },
    SERIF: {
      unUsed: [],
      queue: [],
      viewed: []
    },
    MONOSPACE: {
      unUsed: [],
      queue: [],
      viewed: []
    },
    DISPLAY: {
      unUsed: [],
      queue: [],
      viewed: []
    },
    HANDWRITING: {
      unUsed: [],
      queue: [],
      viewed: []
    }
  };

  constructor(private cat: Category) {}

  removeFont(index: number, filter: string) {
    for (const key in this.PG[filter]) {
      const arr: number[] = this.PG[filter][key];
      let spliceIndex = arr.indexOf(index);
      if (spliceIndex !== -1) {
        arr.splice(spliceIndex, 1);
        return;
      }
    }
  }

  unlike(info: IFontInfo) {
    this.PG[info.style].viewed.push(info.index);
  }

  placeInViewed(currFont: { index: number; style: string }) {
    this.PG[currFont.style].viewed.push(currFont.index);
  }

  pickFont(filter: string): number {
    const index = this.PG[filter].queue.splice(0, 1)[0];
    this.refillQueues();
    return index;
  }

  refreshFilterQueue(filter: string) {
    let indices = this.PG[filter].viewed;
    if (indices.length > 0) {
      indices = this.shuffleArray(indices);
      this.PG[filter].queue = indices;
      this.PG[filter].viewed = [];
      //set filter to empty
      this.cat.filters.setActive(filter);
    }
  }

  refillQueues() {
    let indices: number[] = [];
    for (const pipeGroupKey in this.PG) {
      const pipe: IPipe = this.PG[pipeGroupKey];

      //if queue length is less than threshold
      if (pipe.queue.length < this.queueThresh) {
        //if unused has elements
        if (pipe.unUsed.length > 0) {
          //get and filter loaded indices and concat to array
          let temp: number[] = pipe.unUsed.splice(0, this.queueAddNum);
          temp = this.checkIfLoaded(temp, pipeGroupKey);
          indices = [...indices, ...temp];
          //add indices to loading queue
          this.loadingQueue.add(temp, pipeGroupKey);
        }
        //if queue length is 0 and unUsed is 0
        else if (pipe.queue.length === 0 && pipe.unUsed.length === 0) {
          //set filter to empty
          this.cat.filters.setEmpty(pipeGroupKey);
        }
      }
    }
    //call load fonts
    if (indices.length > 0) {
      this.fontLoader.loadFonts(indices, this.fontLoadedCallBack.bind(this));
    }
  }

  checkIfLoaded(indices: number[], filter: string): number[] {
    return indices.filter(num => {
      if (FontData[num].l > 0) {
        this.PG[filter].queue.push(num);
        return false;
      } else {
        return true;
      }
    });
  }

  fontLoadedCallBack(family) {
    const { index, style } = this.loadingQueue.remove(family);
    this.PG[style].queue.push(index);
  }

  shuffleArray(array: number[]) {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return [...array];
  }

  init() {
    this.PG["SANS_SERIF"]["unUsed"] = this.shuffleArray(SANS_SERIF);
    this.PG["SERIF"]["unUsed"] = this.shuffleArray(SERIF);
    this.PG["MONOSPACE"]["unUsed"] = this.shuffleArray(MONOSPACE);
    this.PG["DISPLAY"]["unUsed"] = this.shuffleArray(DISPLAY);
    this.PG["HANDWRITING"]["unUsed"] = this.shuffleArray(HANDWRITING);
  }

  save(): IPipeGroup {
    return { ...this.PG };
  }

  load(pipeGroup: IPipeGroup) {
    this.PG = { ...pipeGroup };
  }
}
