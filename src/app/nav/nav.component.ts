import { Component, OnInit, Input } from "@angular/core";
import { NavService } from "../nav.service";
import {
  trigger,
  transition,
  style,
  animate
} from "../../../node_modules/@angular/animations";

@Component({
  selector: "nav-bar",
  template: `
  <div class="wrapper" [class.active]="active">
    <div class="panel" [class.active]="active">
      <div class="exit" (click)="active=false">
        <i class="fas fa-times"></i>
      </div>
      <div class="blank"></div>
      <div class="section">
        <a 
        routerLink="/upload" 
        class="link" 
        (click)="active=false"
        [class.active]="currUrl==='/upload'"
        >
          Replace Image
          <i class="fas fa-image"></i>
        </a>
        <a 
        routerLink="/canvas-editor" 
        class="link" 
        (click)="active=false"
        [class.active]="currUrl==='/canvas-editor'"
        >
          Layout
          <i class="far fa-object-ungroup" ></i>
        </a>
        <a 
        routerLink="/flip-or-flop" 
        class="link"
        (click)="active=false" 
        [class.disabled]="fofDisabled"
        [class.active]="currUrl==='/flip-or-flop'"
        >
          Flip or Flop
          <i class="fas fa-fire"></i>
        </a>
        <a 
        routerLink="/font-collection" 
        class="link" 
        (click)="active=false"
        [class.disabled]="fcDisabled"
        [class.active]="currUrl==='/font-collection'" 
        >
          Liked Fonts
          <span>{{totalLikes}}</span>
        </a>
      </div>
      <div class="section subField">
        More Tools Coming Soon!
        <button class="blue" (click)="showSubscribe = true">
          Subscribe     
        </button>
        <a href="mailto:contact@thestudiobros.com">
          <button class="dark">
          Contact     
          </button>
        </a>
      </div>
    </div>
    <div class="exitInvis" (click)="active=false"></div>
    <ng-container *ngIf="showSubscribe===true" >
        <subscribe-form 
        @fadeInOut
        (exit)="closeSub()"
        ></subscribe-form>
    </ng-container>
  </div>

  `,
  styleUrls: ["./nav.component.sass"],
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({
          opacity: 0
        }),
        animate(
          ".3s ease",
          style({
            opacity: 1
          })
        )
      ]),
      transition(":leave", [
        style({
          opacity: 1
        }),
        animate(
          ".3s ease",
          style({
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class NavComponent implements OnInit {
  @Input()
  currUrl: string;
  @Input()
  set boxCount(val: number) {
    if (val > 0) this.fofDisabled = false;
    else this.fofDisabled = true;
  }
  @Input()
  set setTotalLikes(val: number) {
    if (val > 99) this.totalLikes = "99+";
    else this.totalLikes = val;

    if (val > 0) {
      this.fcDisabled = false;
    } else {
      this.fcDisabled = true;
    }
  }
  totalLikes: number | string;
  active: boolean = false;
  fofDisabled: boolean = true;
  fcDisabled: boolean = true;
  showSubscribe: boolean = false;

  constructor(private navServ: NavService) {}

  ngOnInit() {
    //set up nav listener
    this.navServ.openTab.on("open", () => {
      this.active = true;
    });
  }

  ngOnDestroy() {
    //remove listeners
    this.navServ.openTab.removeAllListeners();
  }

  closeSub() {
    this.showSubscribe = false;
  }
}
