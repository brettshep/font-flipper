import { Component, Input } from "@angular/core";
import { FontList } from "./font-list";

@Component({
  selector: "font-list",
  template: `
  <div class="wrapper">
    <div class="catTitle">{{fontList?.category}}</div>

      <div
      *ngFor="let font of fontList?.fonts"
      class="textBlock"
      
      >
        <div class="text" [ngStyle]="{'font-family': font.name}">
          {{fontList?.text}}
        </div>
        <div class="family">
          <a [href]="font.link" target="_blank">
            {{font.name}}
            <i class="fas fa-cloud-download-alt"></i>
          </a>
        </div>
      </div>

  </div>

  `,
  styleUrls: ["./font-list.component.sass"]
})
export class FontListComponent {
  @Input() fontList: FontList;
}
