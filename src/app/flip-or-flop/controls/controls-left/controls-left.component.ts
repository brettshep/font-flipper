import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-left",
  template: `
    <div class="panel" >
      <button *ngIf="!showingLikedFont; else unlike" (click)="dislike.emit()">
        <i class="fas fa-times"></i>
      </button>
      <ng-template #unlike>
        <button (click)="unlikeEmit()" >
          <i class="fas fa-trash-alt"></i>
        </button>
      </ng-template>
    </div>
  `,
  styleUrls: ["./controls-left.component.sass"]
})
export class ControlsLeftComponent {
  @Input() showingLikedFont;
  @Output() dislike = new EventEmitter();
  @Output() unlike = new EventEmitter();
  unlikeEmit() {
    this.unlike.emit();
  }
}
