import { FormsModule } from "@angular/forms";
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FontCollectionComponent } from "./font-collection.component";
import { FontListComponent } from "./font-list/font-list.component";
import { BottomControlsComponent } from "./bottom-controls/bottom-controls.component";
import { TooltipModule } from "ng2-tooltip-directive";
export const ROUTES: Routes = [
  {
    path: "",
    component: FontCollectionComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES),
    FormsModule,
    TooltipModule
  ],
  declarations: [
    FontCollectionComponent,
    FontListComponent,
    BottomControlsComponent
  ]
})
export class FontCollectionModule {}
