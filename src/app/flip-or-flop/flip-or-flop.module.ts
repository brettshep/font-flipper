import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FlipOrFlopComponent } from "./flip-or-flop.component";
import { CanvasImageComponent } from "./canvas-image/canvas-image.component";
import { ControlsRightComponent } from "./controls/controls-right/controls-right.component";
import { ControlsLeftComponent } from "./controls/controls-left/controls-left.component";
import { ControlsBottomComponent } from "./controls/controls-bottom/controls-bottom.component";
import { ControlsTopComponent } from "./controls/controls-top/controls-top.component";
import { TooltipModule } from "ng2-tooltip-directive";
export const ROUTES: Routes = [
  {
    path: "",
    component: FlipOrFlopComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES), TooltipModule],
  declarations: [
    FlipOrFlopComponent,
    CanvasImageComponent,
    ControlsRightComponent,
    ControlsLeftComponent,
    ControlsBottomComponent,
    ControlsTopComponent
  ]
})
export class FlipOrFlopModule {}
