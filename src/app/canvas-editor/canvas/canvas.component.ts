import { IBoxStyles } from "./../../interfaces";
import { StateMachine, events } from "./statemachine";
import { Box } from "./box";
import { Store } from "../../store";
import {
  Component,
  ViewChild,
  ElementRef,
  HostListener,
  Input,
  ChangeDetectionStrategy,
  Output,
  EventEmitter
} from "@angular/core";
import { first } from "../../../../node_modules/rxjs/operators";
import { CatID } from "../../interfaces";

export interface MousePos {
  x: number;
  y: number;
}

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "canvas-comp",
  template: `
  <div #canvasCont class="canvasRel">
    <div class="canvasAbs">
      <canvas 
        #canvas
        (mousedown)="mouseDown($event)"
        (mousemove)="mouseMove($event)"
        (mouseup)="mouseUp($event)"
        (keydown)="keyDown($event)"
        (keyUp)="keyUp($event)"
        >
      </canvas>
    </div>
   <!-- {{SM.currStateString}}
    <a #downloadBtn download = "canvas"><button (click)="downloadCanvas()">Download</button></a>
    -->
  </div>
  `,
  styleUrls: ["./canvas.component.sass"]
})
export class CanvasComponent {
  @ViewChild("canvas")
  canvas: ElementRef;
  @ViewChild("canvasCont")
  canvasCont: ElementRef;
  @ViewChild("downloadBtn")
  downloadBtn: ElementRef;
  ctx: CanvasRenderingContext2D;
  img: HTMLImageElement;
  canvasElem: HTMLCanvasElement;
  canvasContElem: HTMLDivElement;
  canvasFocus: boolean = true;
  textArea: HTMLTextAreaElement;
  boxStyles: IBoxStyles;
  initialBoxes: Box[] = [];
  @Input()
  currBoxCat: CatID;
  @Input()
  set setBoxStyles(val) {
    this.boxStyles = val;
    this.SM.reqDrawAll();
  }
  @Input()
  set imgFile(file: File) {
    if (file) this.setImageFile(file);
  }
  @Input()
  set setInitialBoxes(boxes: Box[]) {
    if (boxes) {
      this.initialBoxes = boxes;
      // this.hasBox.emit();
    }
  }

  // @Output() hasBox = new EventEmitter();
  @Output()
  boxOut = new EventEmitter<Box>();

  resizeTimer: any;
  SM: StateMachine = new StateMachine(this);
  canRatio: number;
  sizeMap: { currHeight; currWidth; baseWidth; baseHeight } = {
    currHeight: null,
    currWidth: null,
    baseWidth: 1000,
    baseHeight: null
  };

  constructor(private store: Store) {}

  modifyStoreBoxCount(num: number) {
    this.store
      .select<number>("boxCount")
      .pipe(first())
      .subscribe(val => {
        this.store.set("boxCount", val + num);
      });
  }
  //get canvas context
  ngAfterViewInit(): void {
    this.ctx = (<HTMLCanvasElement>this.canvas.nativeElement).getContext("2d");
    this.canvasElem = this.canvas.nativeElement;
    this.canvasContElem = this.canvasCont.nativeElement;

    let dpr = window.devicePixelRatio || 1,
      bsr =
        (this.ctx as any).webkitBackingStorePixelRatio ||
        (this.ctx as any).mozBackingStorePixelRatio ||
        (this.ctx as any).msBackingStorePixelRatio ||
        (this.ctx as any).oBackingStorePixelRatio ||
        (this.ctx as any).backingStorePixelRatio ||
        1;

    this.canRatio = dpr / bsr;
    this.ctx.setTransform(this.canRatio, 0, 0, this.canRatio, 0, 0);

    this.textArea = document.querySelector("textarea");

    //draw inital boxes if there are any
    setTimeout(() => {
      this.SM.createInitalBoxes(this.initialBoxes);
    }, 0);
  }

  //set image file on input
  setImageFile(file: File | string) {
    //reader load function
    this.img = new Image();
    //on image load
    this.img.onload = () => {
      this.sizeMap.baseHeight = (this.img.height * 1000) / this.img.width;
      this.resizeCanvas();
    };
    if (typeof file === "string") {
      this.img.src = file;
    } else {
      //set img src
      this.img.src = URL.createObjectURL(file);
    }
  }

  downloadCanvas() {
    console.log("downloading");
    let edge = window.navigator.msSaveOrOpenBlob;
    if (edge) {
      let blob = (this.canvasElem as any).msToBlob();
      window.navigator.msSaveOrOpenBlob(blob, "canvas.png");
    } else {
      const aElem: HTMLLinkElement = this.downloadBtn.nativeElement;
      aElem.href = this.canvasElem.toDataURL("image/png");
    }
  }

  @HostListener("document:keydown", ["$event"])
  keyDown(e: KeyboardEvent) {
    if (document.activeElement !== this.textArea) {
      if (e.shiftKey) {
        this.SM.shiftPress = true;
      }
      this.SM.sendEvent(events.keyDown, e);
    }
  }
  @HostListener("document:keyup", ["$event"])
  keyUp(e: KeyboardEvent) {
    if (document.activeElement !== this.textArea) {
      if (e.shiftKey) {
        this.SM.shiftPress = false;
      }
    }
  }
  @HostListener("document:mousedown", ["$event"])
  mouseDownDoc(e: MouseEvent) {
    this.canvasFocus =
      e.target === this.canvasElem ||
      e.target === this.textArea ||
      (e.target as HTMLElement).classList.contains("styleSelector") ||
      (e.target as HTMLElement).classList.contains("alignControls");
    //deselect box if there is one
    if (!this.canvasFocus && this.img) {
      this.SM.changeCurrBox(null);
      this.SM.reqDrawAll();
    }
  }
  //#region --------RESIZE-------------
  @HostListener("window:resize")
  callResizeCanvas() {
    if (!this.img) return;
    this.resizeCanvas();
  }
  resizeCanvas() {
    //get initial height
    const computedStyle = getComputedStyle(this.canvasContElem);

    const maxHeight =
      this.canvasContElem.clientHeight -
      (parseFloat(computedStyle.paddingTop) +
        parseFloat(computedStyle.paddingBottom));

    const maxWeight =
      this.canvasContElem.clientWidth -
      (parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight));

    let height = maxHeight;

    //get width and ratio
    const ratio = this.img.width / this.img.height;
    let width = height * ratio;
    if (width > maxWeight) {
      width = maxWeight;
      height = width / ratio;
    }

    //set canvas elem width and height
    this.canvasElem.height = height * this.canRatio;
    this.canvasElem.width = width * this.canRatio;
    //set css width
    this.canvasElem.style.width = `${width}px`;
    this.canvasElem.style.height = `${height}px`;
    //set size map currwidth
    this.sizeMap.currHeight = this.canvasElem.height;
    this.sizeMap.currWidth = this.canvasElem.width;
    this.SM.reqDrawAll();
  }
  //#endregion

  //#region ---------MOUSE EVENTS-----------
  mouseUp(e: MouseEvent) {
    if (e.button != 0) return;
    this.SM.sendEvent(events.mouseUp, e);
  }

  mouseDown(e) {
    if (e.button != 0) return;
    this.SM.sendEvent(events.mouseDown, e);
  }

  mouseMove(e) {
    if (e.button != 0) return;
    this.SM.sendEvent(events.mouseMove, e);
  }

  //#endregion

  get boxes() {
    return this.SM.boxes;
  }
}
