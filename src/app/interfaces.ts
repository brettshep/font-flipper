import { FontList } from "./font-collection/font-list/font-list";
import { Category } from "./flip-or-flop/category/category";
import { Alignments } from "./canvas-editor/canvas/box";

export interface IBoxStyle {
  alignment: Alignments;
  fontSize: number;
  letterSpacing: number;
  lineHeight: number;
  rgba: string;
  hslaObj: { h; s; l; a };
}

export interface IBoxStyles {
  0: IBoxStyle;
  1: IBoxStyle;
  2: IBoxStyle;
}

export interface IFontFamily {
  f: string;
  v: string[];
  l: number;
  s: string;
}

export interface IPipe {
  unUsed: number[];
  queue: number[];
  viewed: number[];
}

export interface IPipeGroup {
  SANS_SERIF: IPipe;
  SERIF: IPipe;
  MONOSPACE: IPipe;
  DISPLAY: IPipe;
  HANDWRITING: IPipe;
}

export interface IFilterStatusGroup {
  SANS_SERIF: FilterStatus;
  MONOSPACE: FilterStatus;
  DISPLAY: FilterStatus;
  SERIF: FilterStatus;
  HANDWRITING: FilterStatus;
}

export interface ICatSave {
  fonts: any;
  filters: any;
  liked: any;
  currFont: { index: number; style: string };
}

export interface ICatSaveGroup {
  0: ICatSave;
  1: ICatSave;
  2: ICatSave;
}

export interface ICatGroup {
  0: Category;
  1: Category;
  2: Category;
}
export interface IFontListGroup {
  0: FontList;
  1: FontList;
  2: FontList;
}

export interface IFontInfo {
  name: string;
  index: number;
  style: string;
}

export interface ILoadingQueue {
  name: string;
  index: number;
  style: string;
}

export enum FilterStatus {
  disabled = 0,
  active,
  empty
}

export enum CatID {
  h1 = 0,
  h2,
  body
}
