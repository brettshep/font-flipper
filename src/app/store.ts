import { Box } from "./canvas-editor/canvas/box";
import { Observable, BehaviorSubject } from "rxjs";
import { pluck, distinctUntilChanged } from "rxjs/operators";
import { IBoxStyles, ICatSaveGroup, CatID } from "./interfaces";

export interface State {
  image: File;
  boxes: Box[];
  boxStyles: IBoxStyles;
  catsUsed: CatID[];
  boxCount: number;
  catSaveGroup: ICatSaveGroup;
  [key: string]: any;
}

const state: State = {
  image: undefined,
  boxes: undefined,
  boxStyles: undefined,
  catsUsed: undefined,
  boxCount: 0,
  catSaveGroup: undefined
};

export class Store {
  private subject = new BehaviorSubject<State>(state);
  private store = this.subject.asObservable().pipe(distinctUntilChanged());

  get value() {
    return this.subject.value;
  }

  select<T>(name: string): Observable<T> {
    return this.store.pipe(pluck(name));
  }

  set(name: string, state: any) {
    this.subject.next({ ...this.value, [name]: state });
    //console.log(this.value);
  }
}
