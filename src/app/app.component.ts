import { Box } from "./canvas-editor/canvas/box";
import { NavService } from "./nav.service";
import { CompName } from "./routing.module";
import { Component } from "@angular/core";
import { RouterOutlet, Router, NavigationEnd } from "@angular/router";
import { Subscription, Observable } from "rxjs";
import { Store } from "./store";
import { ICatSaveGroup } from "./interfaces";
import {
  trigger,
  transition,
  group,
  query,
  style,
  animate
} from "@angular/animations";
@Component({
  selector: "app-root",
  template: `
  <div class="page" [@routeAnimation]="getDepth(myOutlet)">
  <nav-bar 
  [currUrl]="currUrl"
  [boxCount]="(boxCount$ | async)"
  [setTotalLikes]="totalLikes"
  ></nav-bar>
    <router-outlet #myOutlet="outlet"></router-outlet>
    
    <div class="contactCont" *ngIf="!showMenu && currUrl==='/upload'">
      <span><a href="mailto:contact@thestudiobros.com">Contact</a></span>
      <span>
        <a href="https://www.dropbox.com/sh/8zq81japxocf613/AADSMt6z4GJcJvKBnJBwVpQUa?dl=0" target="_blank">
        Press Kit
        </a>
      </span>
    </div>
   
    <div class="privacy" *ngIf="!showMenu && currUrl==='/upload'">
      <span>
        <a routerLink="/privacy-policy">
        Privacy Policy
        </a>
      </span>
      <span>
        <a routerLink="/terms">
        Terms & Conditions
        </a>
      </span>
    </div>
   
    <div class="menuBtn" *ngIf="showMenu" (click)="openNav()">
      <i class="fas fa-bars"></i>
        <div *ngIf="totalLikes !== 0" class="likeCount">
          {{totalLikes}} 
          <i class="fas fa-heart"></i></div>
        <div class="pointer"></div>
    </div>
    <div class="help" *ngIf="showMenu" (mouseenter)="showHelp()" (mouseleave)="hideHelp()">
        <i class="fas fa-question-circle"><div class="helpNote">Hover for Help</div></i>
    </div>

    <ng-container *ngIf="helpHover === 'canvasEditor'" >
      <div class="center helpBox mW600" @fadeInOut > 
        <h1>Shortcuts <i class="fas fa-keyboard"></i></h1>   
        <span><span class="bolden">Move Text Box:</span> Click + Drag</span>
        <span><span class="bolden">Nudge Text Box:</span> Arrow Keys (optional Shift key)</span>
        <span><span class="bolden">Delete Text Box:</span> Delete/Backspace</span>
      </div>
      <div class="top helpBox mW600" @fadeInOut >
        <h1>Alignment <i class="fas fa-align-right"></i></h1>
        Align text boxes to the canvas, with padding options
      </div>
      <div class="right helpBox mW275" @fadeInOut >
        <h1>Style Options <i class="fas fa-sliders-h"></i></h1>
        Controls for the selected Style
      </div>
      <div class="bottom helpBox mW600" @fadeInOut >
        <h1>Text Box <i class="fas fa-font"></i></h1>
        Type here to add text while a text box is selected
      </div>
      <div class="left helpBox mW275" @fadeInOut > 
        <h1>Styles <i class="fas fa-palette"></i></h1>
        Use these to assign text boxes different styles
      </div>   
    </ng-container>
    <ng-container *ngIf="helpHover === 'fof'" >
      <div class="center helpBox mW600" @fadeInOut > 
        <h1>Shortcuts <i class="fas fa-keyboard"></i></h1>   
        <span><span class="bolden">Dislike:</span> Left Arrow</span>
        <span><span class="bolden">Like:</span> Right Arrow</span>
        <span><span class="bolden">Delete Liked Font:</span> Select Font + Delete/Backspace</span>
      </div>
      <div class="top helpBox mW600" @fadeInOut >
        <h1>Filters <i class="fas fa-filter"></i></h1>
        Select which types of fonts you want to randomly receive 
      </div>
      <div class="right helpBox mW275" @fadeInOut >
        <h1>Like <i class="fas fa-heart"></i></h1>
        Save the current font
      </div>
      <div class="bottom helpBox mW600" @fadeInOut >
        <h1>Liked Fonts <i class="fas fa-font"></i></h1>
        Select your liked fonts to see them on the canvas
      </div>
      <div class="left helpBox mW275" @fadeInOut > 
        <h1>Dislike <i class="fas fa-times"></i></h1>
        Remove the current font
      </div>   
    </ng-container>
    <ng-container *ngIf="helpHover === 'fontCol'" >
      <div class="center helpBox mW600" @fadeInOut > 
        <h1>Download: <i class="fas fa-download"></i></h1>   
        Click the font name to download the font
      </div>
      <div class="bottom helpBox mW600" @fadeInOut > 
        <h1>View Options: <i class="fas fa-eye"></i></h1>
        Change the text displayed
      </div>   
    </ng-container>
  </div>
  `,
  styleUrls: ["./app.component.sass"],
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({
          opacity: 0
        }),
        animate(
          ".3s ease",
          style({
            opacity: 1
          })
        )
      ]),
      transition(":leave", [
        style({
          opacity: 1
        }),
        animate(
          ".3s ease",
          style({
            opacity: 0
          })
        )
      ])
    ]),
    trigger("routeAnimation", [
      //upload to canvas editor
      transition(`${CompName.upload} => ${CompName.canvasEditor}`, [
        query(":enter", style({ transform: "translateY(100%)" })),
        query(
          ":enter , :leave",
          style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
          { optional: true }
        ),
        group([
          query(
            ":leave",
            [animate(".5s ease", style({ transform: "translateY(-100%)" }))],
            { optional: true }
          ),
          query(":enter", [
            animate(".5s ease", style({ transform: "translateY(0%)" }))
          ])
        ])
      ]),
      //any to upload
      transition(`* => ${CompName.upload}`, [
        query(":enter", style({ transform: "translateY(-100%)" })),
        query(
          ":enter , :leave",
          style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
          { optional: true }
        ),
        group([
          query(
            ":leave",
            [animate(".5s ease", style({ transform: "translateY(100%)" }))],
            { optional: true }
          ),
          query(":enter", [
            animate(".5s ease", style({ transform: "translateY(0%)" }))
          ])
        ])
      ]),
      //canvas editor to fliporflop
      transition(`${CompName.canvasEditor} => ${CompName.flipOrFlop}`, [
        query(":enter", style({ transform: "translateY(100%)" })),
        query(
          ":enter , :leave",
          style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
          { optional: true }
        ),
        group([
          query(
            ":leave",
            [animate(".5s ease", style({ transform: "translateY(-100%)" }))],
            { optional: true }
          ),
          query(":enter", [
            animate(".5s ease", style({ transform: "translateY(0%)" }))
          ])
        ])
      ]),
      //flip or flop to canvas editor
      transition(`${CompName.flipOrFlop} => ${CompName.canvasEditor}`, [
        query(":enter", style({ transform: "translateY(-100%)" })),
        query(
          ":enter , :leave",
          style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
          { optional: true }
        ),
        group([
          query(
            ":leave",
            [animate(".5s ease", style({ transform: "translateY(100%)" }))],
            { optional: true }
          ),
          query(":enter", [
            animate(".5s ease", style({ transform: "translateY(0%)" }))
          ])
        ])
      ]),
      //font collection to any
      transition(`${CompName.fontCollection} => *`, [
        query(":enter", style({ transform: "translateX(100%)" })),
        query(
          ":enter , :leave",
          style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
          { optional: true }
        ),
        group([
          query(
            ":leave",
            [animate(".5s ease", style({ transform: "translateX(-100%)" }))],
            { optional: true }
          ),
          query(":enter", [
            animate(".5s ease", style({ transform: "translateX(0%)" }))
          ])
        ])
      ]),
      //any to font Collection
      transition(`* => ${CompName.fontCollection}`, [
        query(":enter", style({ transform: "translateX(-100%)" }), {
          optional: true
        }),
        query(
          ":enter , :leave",
          style({ position: "absolute", top: 0, left: 0, right: 0, bottom: 0 }),
          { optional: true }
        ),
        group([
          query(
            ":leave",
            [animate(".5s ease", style({ transform: "translateX(100%)" }))],
            { optional: true }
          ),
          query(
            ":enter",
            [animate(".5s ease", style({ transform: "translateX(0%)" }))],
            { optional: true }
          )
        ])
      ])
    ])
  ]
})
export class AppComponent {
  routerSub$: Subscription;
  likedFontsSub$: Subscription;
  boxCount$: Observable<number>;
  currUrl: string;
  totalLikes: number | string;
  showMenu = false;
  helpHover: string = "";
  constructor(
    private navServ: NavService,
    private router: Router,
    private store: Store
  ) {}

  showHelp() {
    switch (this.currUrl) {
      case "/canvas-editor":
        this.helpHover = "canvasEditor";
        break;
      case "/flip-or-flop":
        this.helpHover = "fof";
        break;
      case "/font-collection":
        this.helpHover = "fontCol";
        break;

      default:
        break;
    }
  }

  hideHelp() {
    this.helpHover = "";
  }

  ngOnInit() {
    //routes
    this.routerSub$ = this.router.events.subscribe(val => {
      if (val instanceof NavigationEnd) {
        if (this.router.url === "/canvas-editor") {
          this.showMenu = true;
        }
        this.currUrl = this.router.url;
      }
    });

    //total likes
    this.likedFontsSub$ = this.store
      .select<ICatSaveGroup>("catSaveGroup")
      .subscribe(val => {
        if (val) {
          let total: number = 0;
          for (const catKey in val) {
            if (val[catKey]) {
              total += val[catKey].liked.fonts.length;
            }
          }
          this.totalLikes = total;
        } else this.totalLikes = 0;
      });

    //box count
    this.boxCount$ = this.store.select<number>("boxCount");
  }

  ngOnDestroy() {
    //remove listeners
    this.routerSub$.unsubscribe();
    this.likedFontsSub$.unsubscribe();
  }

  openNav() {
    this.navServ.openTab.emit("open");
  }

  getDepth(outlet: RouterOutlet) {
    let name = outlet.activatedRouteData["name"];
    if (name) return name;
    else return -1;
  }
  //${CompName.canvasEditor} => ${CompName.flipOrFlop}
}
