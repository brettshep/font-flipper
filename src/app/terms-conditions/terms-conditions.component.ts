import { Component, OnInit } from "@angular/core";

@Component({
  selector: "terms-conditions",
  template: `
  <div class="wrapper">
    <div class="topBar">
      <a routerLink="/home"><i class="fas fa-home"></i>Home</a>
    </div>
    <iframe src="assets/HTML/terms-conditions.html" frameborder="0"></iframe>
  </div>
  
  `,
  styleUrls: ["./terms-conditions.component.sass"]
})
export class TermsConditionsComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
