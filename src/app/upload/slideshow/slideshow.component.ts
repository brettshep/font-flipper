import { Component } from "@angular/core";
import {
  trigger,
  transition,
  style,
  animate
} from "../../../../node_modules/@angular/animations";

@Component({
  selector: "slideshow",
  template: `
    <div class="wrapper">
      <div class="imageCont">
        <div 
          class="arrow"
          (click)="previous()">
          <i class="fas fa-chevron-left"></i>
        </div>
      
        <div class="imageCont"  >
          <ng-container *ngFor="let image of images; index as i">
            <div class="image"
              *ngIf="i === activeIndex"
              [ngStyle]="{'background-image': 'url(' + image + ')'}" 
              @fadeInOut  
            >
            </div>
          </ng-container>
        </div>
      
        <div 
          class="arrow" 
          (click)="next()">
          <i class="fas fa-chevron-right"></i>
        </div>
      </div>
      <div class="dots">
        <ng-container *ngFor="let image of images; index as i">
          <span 
          class="dot" 
          [class.active]="i === activeIndex"
          (click)="goTo(i)"
          >
          </span>
        </ng-container>
      </div>
    </div>
  `,
  styleUrls: ["./slideshow.component.sass"],
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({
          opacity: 0
        }),
        animate(
          ".3s ease",
          style({
            opacity: 1
          })
        )
      ]),
      transition(":leave", [
        style({
          opacity: 1
        }),
        animate(
          ".3s ease",
          style({
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class SlideshowComponent {
  constructor() {}
  images: string[] = [
    "/assets/GIFs/0-upload.gif",
    "/assets/GIFs/1-textbox.gif",
    "/assets/GIFs/2-style.gif",
    "/assets/GIFs/3-filters.gif",
    "/assets/GIFs/4-flipflop.gif",
    "/assets/GIFs/5-download.gif"
  ];
  activeIndex = 0;

  next() {
    this.activeIndex++;
    if (this.activeIndex === this.images.length) this.activeIndex = 0;
  }

  previous() {
    this.activeIndex--;
    if (this.activeIndex < 0) this.activeIndex = this.images.length - 1;
  }

  goTo(index) {
    this.activeIndex = index;
  }
}
