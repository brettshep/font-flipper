import { ILoadingQueue } from "../../interfaces";

export class LoadingQueue {
  queue: ILoadingQueue[] = [];

  add(indices: number[], style: string) {
    indices.forEach(num => {
      this.queue.push({ name: FontData[num].f, index: num, style });
    });
  }

  remove(family: string): { index; style } {
    let font: { index; style } = null;
    this.queue.forEach((obj, i) => {
      if (family === obj.name) {
        this.queue.splice(i, 1);
        font = { index: obj.index, style: obj.style };
        return;
      }
    });
    return font;
  }
}
