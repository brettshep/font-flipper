import {
  Component,
  Output,
  EventEmitter,
  Input,
  ChangeDetectionStrategy
} from "@angular/core";
import { Box } from "../../canvas/box";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-top",
  template: `
    <div class="topPanel" [class.active]="box">
      <div class="alignBtn alignControls" (click)="alignBox('left')" tooltip="Left" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/alignLeft.png" alt="align">
      </div>
      <div class="alignBtn alignControls" (click)="alignBox('vCenter')" tooltip="Horizontal Center" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/alignVCenter.png" alt="align">
      </div>
      <div class="alignBtn alignControls" (click)="alignBox('right')" tooltip="Right" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/alignRight.png" alt="align">
      </div>
      <div class="alignBtn alignControls" (click)="alignBox('top')" tooltip="Top" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/alignTop.png" alt="align">
      </div>
      <div class="alignBtn alignControls" (click)="alignBox('hCenter')" tooltip="Vertical Center" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/alignHCenter.png" alt="align">
      </div>
      <div class="alignBtn alignControls" (click)="alignBox('bottom')" tooltip="Bottom" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/alignBottom.png" alt="align">
      </div>
      <div class="alignBtn alignControls padding" tooltip="Padding" placement="bottom" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
        <img src="/assets/paddingIcon.png" alt="align">
        <input class="alignControls" [(ngModel)]="padding" (keyup)="lockPadding()" (blur)="inputBlur()" type="number">
      </div>
    </div>
  `,
  styleUrls: ["./controls-top.component.sass"]
})
export class ControlsTopComponent {
  @Output() paddingOut = new EventEmitter<number>();
  padding: number = 0;
  @Input() box: Box;
  alignBox(dir: string) {
    if (this.box) {
      this.box.alignBox(dir);
    }
  }
  lockPadding() {
    if (this.padding < 0) this.padding = 0;
    if (this.padding === null) this.paddingOut.emit(0);
    else this.paddingOut.emit(this.padding);
  }
  inputBlur() {
    if (this.padding === null) this.padding = 0;
  }
}
