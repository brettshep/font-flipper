import { CatFonts } from "./cat-fonts";
import { CatFilters } from "./cat-filters";

import { CatLikes } from "./cat-likes";
import { ICatSave } from "../../interfaces";

export class Category {
  fonts: CatFonts = new CatFonts(this);
  filters: CatFilters = new CatFilters();
  liked: CatLikes = new CatLikes();
  currFont: { index: number; style: string } = { index: null, style: null };

  selectFont(i: number) {
    this.liked.select(i);
  }

  deselectFont() {
    this.liked.deselect();
  }

  likeFont() {
    this.liked.add(this.currFont.index);
  }

  unlikeFont(i: number) {
    this.liked.remove(i);
  }

  dislikeFont() {
    this.fonts.placeInViewed(this.currFont);
  }

  save(): ICatSave {
    return {
      fonts: this.fonts.save(),
      filters: this.filters.save(),
      liked: this.liked.save(),
      currFont: { ...this.currFont }
    };
  }

  load(obj: ICatSave) {
    this.fonts.load(obj.fonts);
    this.filters.load(obj.filters);
    this.liked.load(obj.liked);
    this.currFont = { ...obj.currFont };
  }

  init() {
    this.fonts.init();
  }

  pickFont(): { index: number; style: string } {
    const filter = this.filters.randomActiveFilter;
    if (!filter) {
      console.log("NO FILTERS");
      return;
    } else {
      let index = this.fonts.pickFont(filter);
      if (index || index === 0) {
        this.currFont.index = index;
        this.currFont.style = filter;
      }
    }
  }
}
