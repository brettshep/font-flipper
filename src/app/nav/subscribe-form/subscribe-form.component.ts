import { Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { FormGroup } from "@angular/forms";
import { Component, OnInit, Output, EventEmitter } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";

export interface SubscribeInfo {
  email: string;
  name: string;
}

@Component({
  selector: "subscribe-form",
  template: `
  <section class="wrapper" (click)="bgClose($event)">
    <form  [formGroup]="emailForm" (submit)="onSubmit()" class="emailForm">  
      <div class="exit" >
          <i class="fas fa-times" (click)="close()"></i>
      </div>
      <div class="form-row">
      <label for="formSubFullName">Full Name</label>
        <input id="formSubFullName" class="customInput" type="text" name="fullName" formControlName="name" placeholder="Danny Designer">        
        <div class="formError" *ngIf="nameVal.invalid && nameVal.touched">Please enter your name</div>

        <label for="formSubEmail">Email</label>
        <input id="formSubEmail" class="customInput" type="text" name="email" formControlName="email"  placeholder="dannyman@email.com">
        <div class="formError" *ngIf="emailVal.invalid && emailVal.touched">Please enter a valid email</div>
    
      </div>
      <button [disabled]="!this.emailForm.valid" type="submit">Get Updates</button>
      
      <div class="paymentLoading" *ngIf="loading"></div>
      
      <div class="authError" *ngIf="mailChimpError">{{ mailChimpError }}</div>   
      
      <div class="subSuccess" *ngIf="mailChimpSuccess">{{ mailChimpSuccess }}
        <i class="fas fa-check-circle"></i> 
      </div> 

      <div class="disclaimer" >This subscribes you to the Studio Bros. newsletter for news and updates.</div>  
    </form>
  </section>
  `,
  styleUrls: ["./subscribe-form.component.sass"]
})
export class SubscribeFormComponent implements OnInit {
  @Output()
  exit = new EventEmitter();
  emailForm: FormGroup;
  loading: boolean;
  mailChimpError: string;
  mailChimpSuccess: string;

  constructor(private fb: FormBuilder, private http: HttpClient) {}

  ngOnInit() {
    //form
    this.emailForm = this.fb.group({
      name: ["", [Validators.required]],
      email: [
        "",
        [Validators.required, Validators.pattern(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)]
      ]
    });
  }

  get nameVal() {
    return this.emailForm.get("name");
  }
  get emailVal() {
    return this.emailForm.get("email");
  }

  close() {
    this.exit.emit();
  }

  bgClose(e: MouseEvent) {
    if ((e.target as HTMLElement).classList.contains("wrapper")) {
      this.close();
    }
  }

  onSubmit() {
    if (!this.emailForm.valid || this.loading) {
      return;
    }

    const name: string = this.nameVal.value;
    const email: string = this.emailVal.value;
    this.loading = true;
    this.handleSubMailChimp({ name, email });
  }

  handleSubMailChimp(subInfo: SubscribeInfo) {
    this.addSubToMailChimp(subInfo.name, subInfo.email).subscribe(
      (res: any) => {
        this.loading = false;
        //see if error
        if (res.result === "error") {
          this.mailChimpSuccess = "";
          this.mailChimpError =
            "There was a problem subscribing to our email list. Perhaps you are already subscribed?";
        } else {
          this.mailChimpError = "";
          this.mailChimpSuccess = "Awesome! Thank you for subscribing.";
          //facebook track lead form mailchimp subscribe
          fbq("track", "Lead");
        }
      },
      error => {
        console.error(error);
        this.mailChimpError = "Something went wrong, please try again later.";
        this.loading = false;
      }
    );
  }

  addSubToMailChimp(name: string, email: string) {
    //call mailchimp
    const endpoint =
      "https://app.us18.list-manage.com/subscribe/post-json?u=237d508aa998a154ba3023db7&amp;id=5ae7dd1eb3&";

    //extract name
    let nameArr = name.trim().split(" ");
    let firstName = nameArr.slice(0, 1).join(" ");
    let lastName = "";
    if (nameArr.length !== 1) {
      lastName = nameArr.slice(-1).join(" ");
    }

    //params
    const params = new HttpParams()
      .set("FNAME", firstName)
      .set("LNAME", lastName)
      .set("EMAIL", email)
      .set("SDATE", this.currentDate)
      .set("b_123abc123abc123abc123abc123abc123abc", ""); // hidden input name
    const mailChimpUrl = endpoint + params.toString();

    //call jsonp api
    return this.http.jsonp(mailChimpUrl, "c");
  }

  get currentDate() {
    let today: any = new Date();
    let dd: any = today.getDate();
    let mm: any = today.getMonth() + 1; //January is 0!
    let yyyy: any = today.getFullYear();
    if (dd < 10) dd = "0" + dd;
    if (mm < 10) mm = "0" + mm;
    today = mm + "/" + dd + "/" + yyyy;
    return today;
  }
}
