import { Observable, Subscription } from "rxjs";
import { IBoxStyle, IBoxStyles, CatID } from "../interfaces";
import { Box, Alignments } from "./canvas/box";
import { Component, ViewChild } from "@angular/core";
import { Store } from "../store";
import { CanvasComponent } from "./canvas/canvas.component";
import { Router, NavigationStart } from "@angular/router";
import { take } from "rxjs/operators";

@Component({
  selector: "canvas-editor",
  template: `
  <div class="wrapper" *ngIf="($imgFile | async) as image">
  <!-- BOTTOM-->
    <controls-top 
      [box]="currBox"
      (paddingOut)="setPadding($event)"
      class="top"
    ></controls-top> 
  <!-- LEFT SIDE -->
    <controls-left 
      class="left sidePanel" 
      [boxCat]="currStyleCat"
      (currStyleCat)="setCurrStyleCat($event)"
    ></controls-left>

  <!-- CENTER CANVAS -->
    <canvas-comp #canvasComp    
      class="center"
      [imgFile]="image"
      [setBoxStyles]="boxStyles"
      [currBoxCat]="currStyleCat"
      [setInitialBoxes]="($boxes | async)"
      (boxOut)="changeBox($event)"
      >      
    </canvas-comp>     

    <!-- RIGHT SIDE -->
      <controls-right  
        class="right sidePanel"
        [setBoxStyle]="currBoxStyle"
        (boxStyleChange)="boxStyleChange($event)"
      ></controls-right>

    <!-- BOTTOM-->
    <controls-bottom 
      [setText]="currBoxText"
      [box]="currBox"
      [boxCount]="($boxCount | async)"
      (outText)="changeBoxText($event)"
      (nextPage)="handleNextPage()"
      class="bottom"
    ></controls-bottom> 
  </div>
  `,
  styleUrls: ["./canvas-editor.component.sass"]
})
export class CanvasEditorComponent {
  @ViewChild(CanvasComponent) canvasComp: CanvasComponent;
  $imgFile: Observable<File>;
  $boxes: Observable<Box[]>;
  $boxCount: Observable<number>;
  routeSub: Subscription;

  defaultBoxStyle: IBoxStyle = {
    alignment: Alignments.left,
    fontSize: 32,
    letterSpacing: 0,
    lineHeight: 0,
    rgba: "rgba(0,0,0,1)",
    hslaObj: { h: 0, s: 0, l: 0, a: 100 }
  };
  currBox: Box = null;
  currBoxText: string = "";
  currStyleCat: CatID = CatID.h1;
  boxStyles: IBoxStyles = {
    0: this.defaultBoxStyle,
    1: this.defaultBoxStyle,
    2: this.defaultBoxStyle
  };
  currBoxStyle: IBoxStyle = this.boxStyles[0];
  constructor(private store: Store, private router: Router) {}

  ngOnInit() {
    //set image
    this.$imgFile = this.store.select("image");
    //set box styles
    this.store
      .select<IBoxStyles>("boxStyles")
      .pipe(take(1))
      .subscribe((val: IBoxStyles) => {
        if (val) {
          this.boxStyles = { ...val };
          this.currBoxStyle = this.boxStyles[0];
        }
      });
    //set boxes
    this.$boxes = this.store.select<Box[]>("boxes");

    //set boxCount
    this.$boxCount = this.store.select<number>("boxCount");

    //before leaving page save data
    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        // save your data
        this.saveDataToStore();
      }
    });
  }

  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }

  saveDataToStore() {
    if (this.canvasComp) {
      let usedBoxCats: CatID[] = [];
      this.canvasComp.boxes.forEach(box => {
        if (!usedBoxCats.includes(box.boxCat)) {
          usedBoxCats.push(box.boxCat);
        }
      });
      usedBoxCats.sort();

      this.store.set("catsUsed", [...usedBoxCats]);
      this.store.set("boxStyles", { ...this.boxStyles });
      this.store.set("boxes", [...this.canvasComp.boxes]);
    }
  }

  handleNextPage() {
    this.router.navigate(["/flip-or-flop"]);
  }

  setPadding(e: number) {
    this.canvasComp.SM.padding.val = e;
    this.canvasComp.SM.reqDrawAll();
  }

  boxStyleChange(e: IBoxStyle) {
    this.boxStyles[this.currStyleCat] = e;
    this.canvasComp.SM.reqDrawAll();
  }

  //change box text
  changeBoxText(e: string) {
    if (this.currBox) {
      this.currBox.text = e;
      this.canvasComp.SM.reqDrawAll();
    }
  }

  //set local and box category
  setCurrStyleCat(e: CatID) {
    //set local cat
    this.currStyleCat = e;

    //set style from cat
    this.currBoxStyle = { ...this.boxStyles[e] };
    //set cat on box
    if (this.currBox) {
      this.currBox.boxCat = e;
      this.canvasComp.SM.reqDrawAll();
    }
  }

  changeBox(box: Box | null) {
    this.currBox = box;
    if (this.currBox) {
      this.currStyleCat = box.boxCat;
      //set style from cat
      this.currBoxStyle = { ...this.boxStyles[box.boxCat] };
      this.currBoxText = box.text;
    } else this.currBoxText = "";
  }
}
