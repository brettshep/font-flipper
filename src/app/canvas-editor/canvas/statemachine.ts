import { CanvasComponent, MousePos } from "./canvas.component";
import { Box, BoxType } from "./box";
export enum states {
  none = 0,
  checkForBox,
  mouseDown,
  testTravel,
  createStillText,
  drawBox,
  drag,
  resize,
  exitDrawBox,
  keyDown
}

export enum events {
  mouseDown = 0,
  mouseMove,
  mouseUp,
  boxFound,
  boxFoundResize,
  boxNotFound,
  stillTextCreated,
  traveled,
  resizeTrue,
  resizeFalse,
  exitDrawBox,
  keyDown,
  keyRelease,
  deselectBox
}

interface State {
  state: states;
  edges: { event: events; state: states }[];
}

export class StateMachine {
  constructor(private cc: CanvasComponent) {}
  state: states = states.none;
  stateArray: State[] = [
    //none state
    {
      state: states.none,
      edges: [
        { event: events.mouseDown, state: states.checkForBox },
        { event: events.keyDown, state: states.keyDown },
        { event: events.mouseMove, state: states.none }
      ]
    },

    //check for box state
    {
      state: states.checkForBox,
      edges: [
        { event: events.boxNotFound, state: states.mouseDown },
        { event: events.boxFound, state: states.drag },
        { event: events.boxFoundResize, state: states.resize },
        { event: events.deselectBox, state: states.none }
      ]
    },
    //mousedown state
    {
      state: states.mouseDown,
      edges: [
        { event: events.mouseMove, state: states.testTravel },
        { event: events.mouseUp, state: states.createStillText }
      ]
    },
    //testTravel state
    {
      state: states.testTravel,
      edges: [
        { event: events.traveled, state: states.drawBox },
        { event: events.mouseUp, state: states.createStillText },
        { event: events.mouseMove, state: states.testTravel }
      ]
    },

    //create still text state
    {
      state: states.createStillText,
      edges: [{ event: events.stillTextCreated, state: states.none }]
    },

    //drawbox state
    {
      state: states.drawBox,
      edges: [
        { event: events.mouseUp, state: states.exitDrawBox },
        { event: events.mouseMove, state: states.drawBox }
      ]
    },
    //drag state
    {
      state: states.drag,
      edges: [
        { event: events.mouseMove, state: states.drag },
        { event: events.mouseUp, state: states.none }
      ]
    },
    //resize state
    {
      state: states.resize,
      edges: [
        { event: events.mouseMove, state: states.resize },
        { event: events.mouseUp, state: states.none }
      ]
    },
    //exit draw box
    {
      state: states.exitDrawBox,
      edges: [{ event: events.exitDrawBox, state: states.none }]
    },
    //key down
    {
      state: states.keyDown,
      edges: [{ event: events.keyRelease, state: states.none }]
    }
  ];
  functionArr = [
    this.none.bind(this),
    this.checkForBox.bind(this),
    this.mouseDown.bind(this),
    this.testTravel.bind(this),
    this.createStillText.bind(this),
    this.drawBox.bind(this),
    this.drag.bind(this),
    this.resize.bind(this),
    this.exitDrawBox.bind(this),
    this.keyDown.bind(this)
  ];

  sendEvent(event: events, mouseEvent?: any) {
    this.stateArray[this.state].edges.forEach(edge => {
      if (event === edge.event) {
        //change state
        this.state = edge.state;
        //call state function
        this.functionArr[this.state](mouseEvent);
      }
    });
  }

  stateAsString(state: states): string {
    return this.stateStringArr[state];
  }

  get currStateString(): string {
    return this.stateAsString(this.state);
  }

  stateStringArr: string[] = [
    "none",
    "checkForBox",
    "mouseDown",
    "testTravel",
    "createStillText",
    "drawBox",
    "drag",
    "resize",
    "exitDrawBox"
  ];

  //-------STATE VARS---------
  boxes: Box[] = [];
  currBox: Box;
  nudgeAmt: number = 1;
  shiftMultiple: number = 10;
  shiftPress: boolean = false;
  selectedCorner = null;
  initialTravelPos = null;
  travelThreshold: number = 50;
  padding: { val: number } = { val: 0 };
  //-------STATE FUNCTIONS---------
  none(e: MouseEvent) {
    this.cc.canvasElem.style.cursor = "pointer";
    if (e && this.currBox) {
      const pos = this.getMousePos(e);
      //on corner
      if (this.currBox.boxType === BoxType.resize) {
        const cornerInfo = this.currBox.testIfResize(pos);
        if (cornerInfo.res) {
          const corner = cornerInfo.corner;
          switch (corner) {
            //top left
            case 0:
              this.cc.canvasElem.style.cursor = "nwse-resize";
              break;
            //top right
            case 1:
              this.cc.canvasElem.style.cursor = "nesw-resize";
              break;
            //bottom right
            case 2:
              this.cc.canvasElem.style.cursor = "nwse-resize";
              break;
            //bottom left
            case 3:
              this.cc.canvasElem.style.cursor = "nesw-resize";
              break;
            default:
              break;
          }
        }
      }
    }
  }

  checkForBox(e: MouseEvent) {
    const pos = this.getMousePos(e);
    let boxFound = false;
    let hitCorner = false;
    //see if resizing
    if (this.currBox && this.currBox.boxType === BoxType.resize) {
      const cornerInfo = this.currBox.testIfResize(pos);
      if (cornerInfo.res) {
        this.selectedCorner = cornerInfo.corner;
        hitCorner = true;
        this.sendEvent(events.boxFoundResize, e);
        return;
      }
    }
    //see if selecting box
    this.boxes.forEach(box => {
      if (
        box.x1 <= pos.x &&
        pos.x <= box.x2 &&
        box.y1 <= pos.y &&
        pos.y <= box.y2
      ) {
        this.changeCurrBox(box);
        boxFound = true;
      }
    });
    //hit box
    if (boxFound && !hitCorner) {
      this.currBox.prevDragPos = { x: pos.x, y: pos.y };
      this.sendEvent(events.boxFound, e);
    }
    //deselect box if there is one, else make new
    else {
      if (this.currBox) {
        this.changeCurrBox(null);
        this.reqDrawAll();
        this.sendEvent(events.deselectBox, null);
      } else {
        this.sendEvent(events.boxNotFound, e);
      }
    }
  }

  mouseDown(e: MouseEvent) {
    this.initialTravelPos = e;
  }

  createStillText(e: MouseEvent) {
    const pos = this.getMousePos(e);
    let width, height;
    this.changeCurrBox(
      new Box(
        pos.x,
        pos.y,
        pos.x,
        pos.y,
        this.cc.ctx,
        this.reqDrawAll.bind(this),
        BoxType.static,
        this.cc.boxStyles,
        this.cc.currBoxCat,
        this.canvasHeightWidth.bind(this),
        this.cc.sizeMap,
        this.padding
      )
    );
    this.cc.modifyStoreBoxCount(1);
    this.boxes.push(this.currBox);
    width = this.cc.ctx.measureText(this.currBox.text).width;
    height = this.currBox.fontHeight;
    (this.currBox.x2 = pos.x + width), (this.currBox.y2 = pos.y + height);
    this.reqDrawAll();
    this.sendEvent(events.stillTextCreated);
  }

  testTravel(e: MouseEvent) {
    //create new box
    const pos = this.getMousePos(e);
    const initalPos = this.getMousePos(this.initialTravelPos);
    //did travel, draw box
    if (this.distanceSqrd(initalPos, pos) > this.travelThreshold) {
      this.changeCurrBox(
        new Box(
          initalPos.x,
          initalPos.y,
          initalPos.x,
          initalPos.y,
          this.cc.ctx,
          this.reqDrawAll.bind(this),
          BoxType.resize,
          this.cc.boxStyles,
          this.cc.currBoxCat,
          this.canvasHeightWidth.bind(this),
          this.cc.sizeMap,
          this.padding
        )
      );
      this.cc.modifyStoreBoxCount(1);
      this.boxes.push(this.currBox);
      this.sendEvent(events.traveled, this.initialTravelPos);
    }
  }

  drawBox(e: MouseEvent) {
    const pos = this.getMousePos(e);
    this.currBox.x2 = pos.x;
    this.currBox.y2 = pos.y;
    this.reqDrawAll();
  }

  resize(e: MouseEvent) {
    if (this.currBox) {
      const pos = this.getMousePos(e);
      switch (this.selectedCorner) {
        //top left
        case 0:
          this.currBox.x1 = pos.x;
          this.currBox.y1 = pos.y;
          break;

        //top right
        case 1:
          this.currBox.x2 = pos.x;
          this.currBox.y1 = pos.y;
          break;

        //bottom right
        case 2:
          this.currBox.x2 = pos.x;
          this.currBox.y2 = pos.y;
          break;

        //bottom left
        case 3:
          this.currBox.x1 = pos.x;
          this.currBox.y2 = pos.y;
          break;

        default:
          break;
      }
    }

    this.reqDrawAll();
  }

  drag(e: MouseEvent) {
    const pos = this.getMousePos(e);
    this.currBox.move(pos);
    this.reqDrawAll();
  }

  exitDrawBox() {
    //switch x's
    if (this.currBox.x2 < this.currBox.x1) {
      [this.currBox.x1, this.currBox.x2] = [this.currBox.x2, this.currBox.x1];
    }
    //switch y's
    if (this.currBox.y2 < this.currBox.y1) {
      [this.currBox.y1, this.currBox.y2] = [this.currBox.y2, this.currBox.y1];
    }
    this.sendEvent(events.exitDrawBox);
  }

  keyDown(e: KeyboardEvent) {
    let nudgeAmt = this.nudgeAmt;
    if (this.shiftPress) {
      nudgeAmt *= this.shiftMultiple;
    }
    if (this.currBox) {
      switch (e.keyCode) {
        //left
        case 37:
          this.currBox.nudge(-nudgeAmt, 0);
          break;
        //right
        case 39:
          this.currBox.nudge(nudgeAmt, 0);
          break;
        //down
        case 40:
          this.currBox.nudge(0, nudgeAmt);
          break;
        //up
        case 38:
          this.currBox.nudge(0, -nudgeAmt);
          break;
        //delete
        case 46:
          this.deleteBox();
          break;
        //delete
        case 8:
          this.deleteBox();
          break;
        //deselect box
        case 27:
          this.changeCurrBox(null);
          break;

        default:
          break;
      }
      this.reqDrawAll();
    }
    this.sendEvent(events.keyRelease);
  }

  deleteBox() {
    let index = this.boxes.indexOf(this.currBox);
    this.boxes.splice(index, 1);
    this.cc.modifyStoreBoxCount(-1);
    this.changeCurrBox(null);
  }
  //#region ---------DRAW-----------

  drawImage() {
    this.cc.ctx.drawImage(
      this.cc.img,
      0,
      0,
      this.cc.canvasElem.width,
      this.cc.canvasElem.height
    );
  }

  reqDrawAll() {
    requestAnimationFrame(this.drawAll.bind(this));
  }
  drawPadding() {
    //draw padding if its greater than 0
    let ratio = this.cc.sizeMap.currWidth / this.cc.sizeMap.baseWidth;
    const { height, width } = this.canvasHeightWidth();
    this.cc.ctx.beginPath();
    this.cc.ctx.rect(
      (0 + this.padding.val) * ratio,
      (0 + this.padding.val) * ratio,
      (width - this.padding.val * 2) * ratio,
      (height - this.padding.val * 2) * ratio
    );
    this.cc.ctx.lineWidth = 1;
    this.cc.ctx.strokeStyle = "#0578ff";
    this.cc.ctx.setLineDash([10, 5]);
    this.cc.ctx.stroke();
    this.cc.ctx.setLineDash([]);
  }
  drawAll() {
    //clear
    this.cc.ctx.clearRect(
      0,
      0,
      this.cc.canvasElem.width,
      this.cc.canvasElem.height
    );

    //image
    this.drawImage();

    //padding
    if (this.padding.val > 0) this.drawPadding();

    //boxes
    if (this.boxes.length) {
      this.boxes.forEach(box => {
        if (box === this.currBox) {
          box.draw("#0578ff");
        } else {
          box.draw("rgba(0,0,0,0)");
        }
      });
    }
  }

  //#endregion

  //#region ----------UTILITES------------
  getMousePos(e: MouseEvent) {
    const rect = this.cc.canvasElem.getBoundingClientRect();
    return {
      x:
        (e.clientX - rect.left) *
        this.cc.canRatio *
        (this.cc.sizeMap.baseWidth / this.cc.sizeMap.currWidth),
      y:
        (e.clientY - rect.top) *
        this.cc.canRatio *
        (this.cc.sizeMap.baseWidth / this.cc.sizeMap.currWidth)
    } as MousePos;
  }

  changeCurrBox(box: Box | null) {
    this.currBox = box;
    this.cc.boxOut.emit(box);
  }

  distanceSqrd(p1: MousePos, p2: MousePos): number {
    let dx = p2.x - p1.x;
    let dy = p2.y - p1.y;
    return dx * dx + dy * dy;
  }

  canvasHeightWidth(): { height: number; width: number } {
    return {
      height: this.cc.sizeMap.baseHeight,
      width: this.cc.sizeMap.baseWidth
    };
  }

  createInitalBoxes(boxes: Box[]) {
    boxes.forEach(box => {
      this.boxes.push(
        new Box(
          box.x1,
          box.y1,
          box.x2,
          box.y2,
          this.cc.ctx,
          this.reqDrawAll.bind(this),
          box.boxType,
          this.cc.boxStyles,
          box.boxCat,
          this.canvasHeightWidth.bind(this),
          this.cc.sizeMap,
          this.padding,
          box.text
        )
      );
    });
    this.reqDrawAll();
  }

  //#endregion
}
