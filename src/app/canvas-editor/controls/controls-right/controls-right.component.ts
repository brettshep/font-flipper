import {
  Component,
  ChangeDetectionStrategy,
  Input,
  ViewChild,
  ElementRef,
  ChangeDetectorRef,
  Output,
  EventEmitter
} from "@angular/core";
import { VerticalSliderDirective } from "./color-picker/vertical-slider.directive";
import { IBoxStyle } from "../../../interfaces";
import { Alignments } from "../../canvas/box";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-right",
  template: `
    <div class="panel"> 
      <div class="wrapper">
        <div>
          <div class="align" #align tooltip="Align" placement="left" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
            <div class="box" (click)="toggleAlign()" >
              <i *ngIf="currAlignment===0" class="fas fa-align-left"></i>
              <i *ngIf="currAlignment===1"class="fas fa-align-center"></i>
              <i *ngIf="currAlignment===2"class="fas fa-align-right"></i>
            </div>
            <div class="alignIcons" [class.active]="alignActive" >
              <div class="alignIcon" (click)="setAlignment(0)">
                <i class="fas fa-align-left"></i>
              </div>
              <div class="alignIcon" (click)="setAlignment(1)">
                <i class="fas fa-align-center"></i>
              </div>
              <div class="alignIcon" (click)="setAlignment(2)">
                <i class="fas fa-align-right"></i>
              </div>     
            </div>
          </div>
          <div class="inputCont" tooltip="Font Size" placement="left" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
            <input class="box"
              [class.active]="currSliderName==='fontsize'"
              type="number" 
              [(ngModel)]="fontSize" 
              (input)="fontSizeChange()" 
              (keyup)="fontSizeChange()"
              (focus)="setSliderName('fontsize')"
              (blur)="setSliderName('')"
              >
              px
          </div>
          <div class="inputCont" tooltip="Letter Spacing" placement="left" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
              <input class="box"
                [class.active]="currSliderName==='letterspacing'"
                type="number" 
                [(ngModel)]="letterSpacing" 
                (input)="lineSpaceChange()" 
                (keyup)="lineSpaceChange()"
                (focus)="setSliderName('letterspacing')"
                (blur)="setSliderName('')"
              >
              <i class="fas fa-arrows-alt-h"></i>
            </div>
            <div class="inputCont" tooltip="Line Height" placement="left" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
              <input class="box"
              [class.active]="currSliderName==='lineheight'"
              type="number"
              [(ngModel)]="lineHeightMult" 
              (input)="lineHeightChange()" 
              (keyup)="lineHeightChange()"
              (focus)="setSliderName('lineheight')"
              (blur)="setSliderName('')"
              >
              <i class="fas fa-arrows-alt-v"></i>
            </div>
            <!--Color Popup-->
            <color-picker 
            class="colorPicker" 
            *ngIf="showColorPicker" 
            [class.active]="colorPickerActiveClass"
            (colorsObjOut)="colorChange($event)" 
            (close)="closeColor()"
            [inputColor]="currColorHSLA"
            ></color-picker>
            <div class="box colorPickerCont" tooltip="Color" placement="left" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
              <span [innerHTML]="'&nbsp'"></span>                   
              <div (click)="toggleColor()" class="color" [ngStyle]="currColorBG"></div>
            </div>
        </div>
        <div class="sliderHolder">
          <!--Font Size Slider-->
          <div 
            *ngIf="currSliderName==='fontsize'"
            class="slider"
            VerticalSlider 
            [min]="0" 
            [max]="fontSizeMax" 
            type="range" 
            (value)="sliderSetFontSize($event)" 
            (ref)="setSlider($event,'fontsize')">
            <div class="bar"></div>
            <div class="knob"></div>
          </div>
          <!--Letter Spacing Slider-->
          <div 
            *ngIf="currSliderName==='letterspacing'"
            class="slider" 
            VerticalSlider 
            [min]="0" 
            [max]="letterSpacingMax" 
            type="range" 
            (value)="sliderSetLetterSpacing($event)" 
            (ref)="setSlider($event,'letterspacing')">
            <div class="bar"></div>
            <div class="knob"></div>
          </div>
          <!--Line Space Slider-->
          <div 
            *ngIf="currSliderName==='lineheight'"
            class="slider" 
            VerticalSlider 
            [min]="0" 
            [max]="lineHeightMax" 
            type="range" 
            (value)="sliderSetLineHeight($event)" 
            (ref)="setSlider($event,'lineheight')">
            <div class="bar"></div>
            <div class="knob"></div>
          </div>
        </div>     
      </div>
    </div>
  `,
  styleUrls: ["./controls-right.component.sass"]
})
export class ControlsRightComponent {
  @ViewChild("align") align: ElementRef;
  lineHeightMult: number = 0;
  letterSpacing: number = 0;
  fontSize: number = 16;
  lineHeightMax = 100;
  letterSpacingMax = 100;
  fontSizeMax = 300;
  showColorPicker: boolean = false;
  colorPickerActiveClass: boolean = false;
  currSliderRef: VerticalSliderDirective;
  currSliderName: string;
  alignActive: boolean = false;
  currAlignment: Alignments = Alignments.left;
  listenerBind: any = this.alignListener.bind(this);
  currColorRGBA: string = "rbga(0,0,0,0)";
  currColorHSLA: { h; s; l; a } = null;
  currBoxStyle: IBoxStyle;
  @Input()
  set setBoxStyle(currBoxStyle: IBoxStyle) {
    this.currBoxStyle = { ...currBoxStyle };
    this.lineHeightMult = currBoxStyle.lineHeight;
    this.letterSpacing = currBoxStyle.letterSpacing;
    this.fontSize = currBoxStyle.fontSize;
    this.currAlignment = currBoxStyle.alignment;
    this.currColorRGBA = currBoxStyle.rgba;
    this.currColorHSLA = currBoxStyle.hslaObj;
  }
  @Output() boxStyleChange = new EventEmitter<IBoxStyle>();

  constructor(private cd: ChangeDetectorRef) {}

  ngOnDestroy() {
    document.removeEventListener("mousedown", this.listenerBind);
  }

  closeColor() {
    this.colorPickerActiveClass = false;
    setTimeout(() => {
      this.showColorPicker = false;
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    }, 400);
  }

  toggleAlign() {
    this.alignActive = true;
    document.addEventListener("mousedown", this.listenerBind);
  }

  alignListener(e: MouseEvent) {
    const target = e.target;
    const clickedInside = this.align.nativeElement.contains(target);
    if (!clickedInside) {
      document.removeEventListener("mousedown", this.listenerBind);
      this.alignActive = false;
      if (!this.cd["destroyed"]) {
        this.cd.detectChanges();
      }
    }
  }

  setSliderName(name) {
    this.currSliderName = name;
  }

  setSlider(e, name) {
    this.currSliderRef = e;
    switch (name) {
      case "fontsize":
        this.currSliderRef.setHeight(this.fontSize / 3);
        break;
      case "letterspacing":
        this.currSliderRef.setHeight(this.letterSpacing);
        break;
      case "lineheight":
        this.currSliderRef.setHeight(this.lineHeightMult);
        break;

      default:
        break;
    }
  }

  sliderSetFontSize(val) {
    this.fontSize = val;
    this.fontSizeChange();
  }

  sliderSetLineHeight(val) {
    this.lineHeightMult = val;
    this.lineHeightChange();
  }

  sliderSetLetterSpacing(val) {
    this.letterSpacing = val;
    this.lineSpaceChange();
  }

  fontSizeChange() {
    if (this.fontSize < 0) this.fontSize = 0;
    if (this.fontSize > this.fontSizeMax) this.fontSize = this.fontSizeMax;
    this.currSliderRef.setHeight(this.fontSize / 3);
    this.currBoxStyle.fontSize = this.fontSize;
    this.boxStyleChange.emit(this.currBoxStyle);
  }

  lineHeightChange() {
    if (this.lineHeightMult < 0) this.lineHeightMult = 0;
    if (this.lineHeightMult > this.lineHeightMax)
      this.lineHeightMult = this.lineHeightMax;
    this.currSliderRef.setHeight(this.lineHeightMult);
    this.currBoxStyle.lineHeight = this.lineHeightMult;
    this.boxStyleChange.emit(this.currBoxStyle);
  }

  lineSpaceChange() {
    if (this.letterSpacing < 0) this.letterSpacing = 0;
    if (this.letterSpacing > this.letterSpacingMax)
      this.letterSpacing = this.letterSpacingMax;
    this.currSliderRef.setHeight(this.letterSpacing);
    this.currBoxStyle.letterSpacing = this.letterSpacing;
    this.boxStyleChange.emit(this.currBoxStyle);
  }

  setAlignment(input: Alignments) {
    this.currAlignment = input;
    this.alignActive = false;
    this.currBoxStyle.alignment = input;
    this.boxStyleChange.emit(this.currBoxStyle);
  }

  colorChange(colorObj: { rgba; hsla }) {
    this.currColorRGBA = colorObj.rgba;
    this.currColorHSLA = colorObj.hsla;
    this.currBoxStyle.rgba = colorObj.rgba;
    this.currBoxStyle.hslaObj = colorObj.hsla;
    this.boxStyleChange.emit(this.currBoxStyle);
  }

  toggleColor() {
    this.showColorPicker = true;
    this.colorPickerActiveClass = true;
  }

  get currColorBG() {
    return {
      "background-color": `${this.currColorRGBA}`
    };
  }
}
