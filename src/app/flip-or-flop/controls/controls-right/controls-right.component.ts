import { CatID } from "./../../../interfaces";
import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-right",
  template: `
  <div class="panel" >
    <button *ngIf="!showingLikedFont; else copyToCat" (click)="like.emit()" >
      <i class="fas fa-heart"></i>
    </button>
    <ng-template #copyToCat>
      <div class="copyCatCont">
        <div *ngIf="availCats.length > 0" class="copyText">copy font to...</div>
        <div *ngIf="availCats.includes(0)" class="copyCat" (click)="copyToCatEmit(0)">
          H1
        </div>
        <div *ngIf="availCats.includes(1)" class="copyCat" (click)="copyToCatEmit(1)">
          H2
        </div>
        <div *ngIf="availCats.includes(2)" class="copyCat" (click)="copyToCatEmit(2)">
          Body
        </div>        
      </div>
    </ng-template>
  </div>
  `,
  styleUrls: ["./controls-right.component.sass"]
})
export class ControlsRightComponent {
  @Input() showingLikedFont;
  @Input() availCats;
  @Output() like = new EventEmitter();
  @Output() copyToCat = new EventEmitter<CatID>();
  copyToCatEmit(id: CatID) {
    this.copyToCat.emit(id);
  }
}
