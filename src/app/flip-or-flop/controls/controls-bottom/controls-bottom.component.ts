import { IFontFamily, IFontInfo } from "../../../interfaces";

import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
  Input,
  HostListener
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-bottom",
  template: `
    <div class="bottomPanel">
      <div class="marginWrapper">
      <!--
        <button class="plus box">
          <i class="fas fa-plus"></i>
        </button>
        -->
        <div 
        class="fontCont box" 
        *ngFor="let font of likedFonts"
        (click)="toggleActive(font)"
        [class.active]="activeFont===font"
        [ngStyle]="{'font-family':font.name}"
        >
        {{font.name}}
        </div>
      </div>
    </div>
  `,
  styleUrls: ["./controls-bottom.component.sass"]
})
export class ControlsBottomComponent {
  @Input()
  set setLikedFonts(val: number[]) {
    if (val) {
      this.likedFonts = val.map((index: number) => {
        const family: IFontFamily = FontData[index];
        return { name: family.f, index, style: family.s };
      });
    }
  }
  @Input()
  set setActiveFont(id: number[]) {
    if (id[0] !== null) {
      this.likedFonts.forEach((val, index) => {
        if (val.index === id[0]) {
          this.activeFont = this.likedFonts[index];
          return;
        }
      });
    }
  }
  @Output() showLikedFont = new EventEmitter<IFontInfo>();
  @Output() deselectLikedFont = new EventEmitter();
  @Output() unlikeFont = new EventEmitter<IFontInfo>();
  likedFonts: IFontInfo[] = [];
  activeFont: IFontInfo = null;
  toggleActive(font: IFontInfo) {
    if (this.activeFont !== font) {
      this.activeFont = font;
      this.outputSelectedFont();
    } else {
      this.activeFont = null;
      this.deselectLikedFont.emit();
    }
  }

  outputSelectedFont() {
    this.showLikedFont.emit(this.activeFont);
  }

  @HostListener("document:keydown", ["$event"])
  switchActiveFont(e: KeyboardEvent) {
    const currFontIndex = this.likedFonts.indexOf(this.activeFont);
    if (currFontIndex !== -1) {
      if (e.keyCode === 39 && currFontIndex < this.likedFonts.length - 1) {
        //move active font + 1
        this.activeFont = this.likedFonts[currFontIndex + 1];
        this.outputSelectedFont();
      } else if (e.keyCode === 37 && currFontIndex > 0) {
        //move active font -1
        this.activeFont = this.likedFonts[currFontIndex - 1];
        this.outputSelectedFont();
      }
      //delete
      else if (e.keyCode === 46 || e.keyCode === 8) {
        this.unlikeFont.emit(this.activeFont);
      }
    }
  }
}
