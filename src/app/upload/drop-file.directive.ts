import {
  Directive,
  Output,
  EventEmitter,
  ElementRef,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[DropFile]"
})
export class DropFileDirective {
  @Output() dragOver = new EventEmitter();
  @Output() dragLeave = new EventEmitter();
  @Output() dragDrop = new EventEmitter<File>();
  constructor(private elem: ElementRef) {}

  @HostListener("ondrop", ["$event"])
  ondrop(e: DragEvent) {
    e.preventDefault();
    if (e.dataTransfer.items) {
      for (var i = 0; i < e.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (e.dataTransfer.items[i].kind === "file") {
          let file = e.dataTransfer.items[i].getAsFile();
          this.dragDrop.emit(file);
          return;
        }
      }
    }
  }
  @HostListener("ondragover")
  ondragover() {
    this.dragOver.emit();
  }
  @HostListener("ondragleave")
  ondragout() {
    this.dragLeave.emit();
  }
}
