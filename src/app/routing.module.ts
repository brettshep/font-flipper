import { CanvasEditorGuard } from "./canvas-editor.guard";
import { MobileGuardGuard } from "./mobile-guard.guard";
import { FlipOrFlopGuard } from "./flip-or-flop.guard";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { FontCollectionGuard } from "./font-collection.guard";

export enum CompName {
  upload = 1,
  canvasEditor,
  flipOrFlop,
  fontCollection,
  mobile
}

const routes: Routes = [
  {
    path: "upload",
    data: { name: CompName.upload },
    loadChildren: "./upload/upload.module#UploadModule",
    canActivate: [MobileGuardGuard]
  },
  {
    path: "canvas-editor",
    data: { name: CompName.canvasEditor },
    loadChildren: "./canvas-editor/canvas-editor.module#CanvasEditorModule",
    canActivate: [MobileGuardGuard, CanvasEditorGuard]
  },
  {
    path: "flip-or-flop",
    data: { name: CompName.flipOrFlop },
    loadChildren: "./flip-or-flop/flip-or-flop.module#FlipOrFlopModule",
    canActivate: [MobileGuardGuard, FlipOrFlopGuard]
  },
  {
    path: "font-collection",
    data: { name: CompName.fontCollection },
    loadChildren:
      "./font-collection/font-collection.module#FontCollectionModule",
    canActivate: [MobileGuardGuard, FontCollectionGuard]
  },
  {
    path: "terms",
    loadChildren:
      "./terms-conditions/terms-conditions.module#TermsConditionsModule"
  },
  {
    path: "privacy-policy",
    loadChildren: "./privacy-policy/privacy-policy.module#PrivacyPolicyModule"
  },
  {
    path: "mobile",
    data: { name: CompName.mobile },
    loadChildren: "./mobile/mobile.module#MobileModule"
  },
  { path: "**", redirectTo: "upload", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class RoutingModule {}
