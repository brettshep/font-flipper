import { DrawNoEditCanvas } from "./drawNoEditCanva";
import { Box } from "../../canvas-editor/canvas/box";
import { IBoxStyles } from "../../interfaces";
import {
  Component,
  Input,
  ViewChild,
  ElementRef,
  HostListener,
  ChangeDetectionStrategy
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "canvas-image",
  template: `
  <div #canvasCont class="canvasRel">
  <div class="canvasAbs">
    <canvas #canvas></canvas>
  </div>
</div>
  `,
  styleUrls: ["./canvas-image.component.sass"]
})
export class CanvasImageComponent {
  @Input()
  set image(file) {
    if (file) this.setImageFile(file);
  }
  @Input()
  boxStyles: IBoxStyles;
  @Input()
  boxes: Box[];

  @ViewChild("canvas")
  canvas: ElementRef;
  @ViewChild("canvasCont")
  canvasCont: ElementRef;

  img: HTMLImageElement;
  ctx: CanvasRenderingContext2D;
  canvasElem: HTMLCanvasElement;
  canvasContElem: HTMLDivElement;
  canRatio: number;
  sizeMap: { currHeight; currWidth; baseWidth; baseHeight } = {
    currHeight: null,
    currWidth: null,
    baseWidth: 1000,
    baseHeight: null
  };
  DC: DrawNoEditCanvas = new DrawNoEditCanvas(this);

  // interval = setInterval(() => {
  //   this.DC.reqDrawAll();
  // }, 500);
  ngAfterViewInit() {
    this.ctx = (<HTMLCanvasElement>this.canvas.nativeElement).getContext("2d");
    this.canvasElem = this.canvas.nativeElement;
    this.canvasContElem = this.canvasCont.nativeElement;

    let dpr = window.devicePixelRatio || 1,
      bsr =
        (this.ctx as any).webkitBackingStorePixelRatio ||
        (this.ctx as any).mozBackingStorePixelRatio ||
        (this.ctx as any).msBackingStorePixelRatio ||
        (this.ctx as any).oBackingStorePixelRatio ||
        (this.ctx as any).backingStorePixelRatio ||
        1;

    this.canRatio = dpr / bsr;
    this.ctx.setTransform(this.canRatio, 0, 0, this.canRatio, 0, 0);

    //draw canvas
    this.DC.createBoxes(this.boxes);
  }

  // ngOnDestroy() {
  //   clearInterval(this.interval);
  // }
  setImageFile(file: File) {
    //reader load function
    this.img = new Image();
    //on image load
    this.img.onload = () => {
      this.sizeMap.baseHeight = (this.img.height * 1000) / this.img.width;
      this.resizeCanvas();
    };
    if (typeof file === "string") {
      this.img.src = file;
    } else {
      //set img src
      this.img.src = URL.createObjectURL(file);
    }
  }

  //#region --------RESIZE-------------
  @HostListener("window:resize")
  callResizeCanvas() {
    if (!this.img) return;
    this.resizeCanvas();
  }
  resizeCanvas() {
    //get initial height
    const computedStyle = getComputedStyle(this.canvasContElem);

    const maxHeight =
      this.canvasContElem.clientHeight -
      (parseFloat(computedStyle.paddingTop) +
        parseFloat(computedStyle.paddingBottom));

    const maxWeight =
      this.canvasContElem.clientWidth -
      (parseFloat(computedStyle.paddingLeft) +
        parseFloat(computedStyle.paddingRight));

    let height = maxHeight;

    //get width and ratio
    const ratio = this.img.width / this.img.height;
    let width = height * ratio;
    if (width > maxWeight) {
      width = maxWeight;
      height = width / ratio;
    }

    //set canvas elem width and height
    this.canvasElem.height = height * this.canRatio;
    this.canvasElem.width = width * this.canRatio;
    //set css width
    this.canvasElem.style.width = `${width}px`;
    this.canvasElem.style.height = `${height}px`;
    //set size map currwidth
    this.sizeMap.currHeight = this.canvasElem.height;
    this.sizeMap.currWidth = this.canvasElem.width;
    this.DC.reqDrawAll();
  }
  //#endregion
}
