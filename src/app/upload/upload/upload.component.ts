import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Store } from "../../store";
import { first } from "rxjs/operators";
@Component({
  selector: "upload",
  template: `
      <!-- UPLOAD -->
  <div class="upload">   
    <img src="/assets/logo.png" alt="logo" class="logo"> 
      <ng-container *ngIf="!hasImage">
        <slideshow class="slideshow" ></slideshow>
        <div class="sampleImage" (click)="setSampleImage()">
          Try a Sample Image
          <i class="fas fa-image"></i>
        </div>
      </ng-container>
    <div class="inputCont" [class.replaceImage]="hasImage">     
        <input type="file" (dragover)="ondragover($event)" (dragleave)="ondragout($event)" (drop)="ondrop($event)" (change)="handleImgUpload($event)" title=" "/>
        <img src="/assets/uploadIcon.png" alt="upload">
        <p>Click or Drag to Upload</p>
        <div class="dottedBorder" [class.active]="dragHover"></div>
    </div> 
  </div>
  `,
  styleUrls: ["./upload.component.sass"]
})
export class UploadComponent {
  dragHover: boolean = false;
  hasImage: boolean = false;
  testImgURL: string =
    "https://firebasestorage.googleapis.com/v0/b/font-flipper.appspot.com/o/sample-image-tiny.png?alt=media&token=21a7b6d6-7e57-4f55-a797-885584822357";

  constructor(private store: Store, private router: Router) {}

  ngOnInit() {
    //set box styles
    this.store
      .select<any>("image")
      .pipe(first())
      .subscribe((val: any) => {
        if (val) {
          this.hasImage = true;
        }
      });
  }

  handleImgUpload(e) {
    const files = e.target.files;
    //see if file
    if (files && files[0]) {
      const file = files[0];
      const imageType = /image.*/;
      //see if image file
      if (file.type.match(imageType)) {
        this.setImageFile(file);
      }
    }
  }

  setSampleImage() {
    this.store.set("image", this.testImgURL);
    this.router.navigate(["/canvas-editor"]);
  }

  setImageFile(file: File) {
    this.resizeImage({ file, maxSize: 1200 }).then(val => {
      this.store.set("image", val);
      this.router.navigate(["/canvas-editor"]);
    });
  }

  ondragover(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = "copy";
    this.dragHover = true;
  }

  ondragout(e) {
    e.preventDefault();
    this.dragHover = false;
  }

  ondrop(e: DragEvent) {
    e.preventDefault();
    if (e.dataTransfer.items) {
      for (var i = 0; i < e.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (e.dataTransfer.items[i].kind === "file") {
          let file = e.dataTransfer.items[i].getAsFile();
          const imageType = /image.*/;
          //see if image file
          if (file.type.match(imageType)) {
            this.dragHover = false;
            this.setImageFile(file);
            return;
          }
        }
      }
    }
    this.dragHover = false;
  }

  //resize image
  resizeImage(settings: { maxSize: number; file: File }) {
    const file = settings.file;
    const maxSize = settings.maxSize;
    const reader = new FileReader();
    const image = new Image();
    const canvas = document.createElement("canvas");
    const dataURItoBlob = (dataURI: string) => {
      const bytes =
        dataURI.split(",")[0].indexOf("base64") >= 0
          ? atob(dataURI.split(",")[1])
          : unescape(dataURI.split(",")[1]);
      const mime = dataURI
        .split(",")[0]
        .split(":")[1]
        .split(";")[0];
      const max = bytes.length;
      const ia = new Uint8Array(max);
      for (var i = 0; i < max; i++) ia[i] = bytes.charCodeAt(i);
      return new Blob([ia], { type: mime });
    };
    const resize = () => {
      let width = image.width;
      let height = image.height;

      if (width > height) {
        if (width > maxSize) {
          height *= maxSize / width;
          width = maxSize;
        }
      } else {
        if (height > maxSize) {
          width *= maxSize / height;
          height = maxSize;
        }
      }

      canvas.width = width;
      canvas.height = height;
      canvas.getContext("2d").drawImage(image, 0, 0, width, height);
      let dataUrl = canvas.toDataURL("image/jpeg");
      return dataURItoBlob(dataUrl);
    };

    return new Promise((ok, no) => {
      if (!file.type.match(/image.*/)) {
        no(new Error("Not an image"));
        return;
      }

      reader.onload = (readerEvent: any) => {
        image.onload = () => ok(resize());
        image.src = readerEvent.target.result;
      };
      reader.readAsDataURL(file);
    });
  }
}
