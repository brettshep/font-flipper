export class FontList {
  constructor(public category: string) {}
  customText: string = "";
  boxText: string = "";
  text: string = "";
  fonts: { name: string; link: string }[] = [];
}
