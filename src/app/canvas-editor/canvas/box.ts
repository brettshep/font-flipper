import { IBoxStyles } from "./../../interfaces";
import { MousePos, CanvasComponent } from "./canvas.component";
import { CatID } from "../../interfaces";

export enum Alignments {
  left = 0,
  center,
  right
}
export enum BoxType {
  static = 0,
  resize
}

export class Box {
  constructor(
    public x1,
    public y1,
    public x2,
    public y2,
    private ctx: CanvasRenderingContext2D,
    private drawFunc: Function,
    public boxType: BoxType,
    private boxStyles: IBoxStyles,
    public boxCat: CatID,
    private canvasHeightWidth: Function,
    private sizeMap: { currWidth; baseWidth },
    private padding: { val: number },
    text?: string
  ) {
    if (text) this.text = text;
  }
  baseFontHeight: number = 18;
  height: number;
  width: number;
  prevDragPos: MousePos = { x: null, y: null };
  drawingBox: boolean = true;
  fontFamily: string = "Ubuntu";
  fontWeight: string = "400";
  fontStyle: string = "normal";
  fontSize: number = 35;
  text: string = "My Text...";
  lineHeightMult = 1;
  alignment: Alignments = Alignments.left;
  fontColor: string = "black";
  letterSpacing: number = 0;
  circleRad = 8;
  ratio: number = 0;
  fontData: { x: number; y: number; text: string }[] = [];
  draw(color: string) {
    //get ratio
    this.ratio = this.sizeMap.currWidth / this.sizeMap.baseWidth;
    //clear text array
    this.fontData = [];
    //get offset for drag box
    let w = this.x2 - this.x1;
    let h = this.y2 - this.y1;
    let offsetX = w < 0 ? w : 0;
    let offsetY = h < 0 ? h : 0;

    //set width and height
    this.height = Math.abs(this.y2 - this.y1);
    this.width = Math.abs(this.x2 - this.x1);

    //set styles
    this.setBoxStyles();
    //text
    //this.ctx.textBaseline = "hanging";
    this.ctx.fillStyle = this.fontColor;
    this.ctx.font = `${this.fontStyle} ${this.fontWeight} ${this.fontSize}px ${
      this.fontFamily
    }`;

    //draw resize box and text
    if (this.boxType === BoxType.resize) {
      let fontPos = this.calcResizeAlignment(
        this.x1 + offsetX,
        this.y1 + offsetY
      );
      //draw text
      this.wrapTextBox(this.width, fontPos.x, fontPos.y);
      this.drawResizeText();
      //draw rect
      this.drawRect(
        this.x1 + offsetX,
        this.y1 + offsetY,
        this.width,
        this.height,
        color
      );
    }
    //draw static box and text
    else {
      //draw text and get max-width
      let { numLines, longestLine } = this.wrapText(this.x1, this.y1);
      //get width and height, set x2,y2
      let { width, height } = this.checkHeightWidth(longestLine, numLines);
      let xOff = this.calcStaticAlignment(width);
      this.drawStaticText(xOff);

      //draw rect
      this.drawRect(this.x1, this.y1, width, height, color);
    }

    //circle points
    if (this.boxType === BoxType.resize) {
      this.drawCornerPoints(color, this.x1 + offsetX, this.y1 + offsetY);
    }
  }

  setBoxStyles() {
    let boxStyle = this.boxStyles[this.boxCat];
    this.fontSize = boxStyle.fontSize;
    this.lineHeightMult = 1 + boxStyle.lineHeight * 0.05;
    this.alignment = boxStyle.alignment;
    this.fontColor = boxStyle.rgba;
    this.letterSpacing = boxStyle.letterSpacing;
  }

  drawRect(x1, y1, width, height, color) {
    this.ctx.beginPath();
    this.ctx.rect(
      x1 * this.ratio,
      y1 * this.ratio,
      width * this.ratio,
      height * this.ratio
    );
    this.ctx.lineWidth = 1;
    this.ctx.strokeStyle = color;
    this.ctx.stroke();
  }

  checkHeightWidth(
    longestLine: number,
    numLines: number
  ): { width: number; height: number } {
    let width, height;
    let lineHeight =
      this.fontHeight * (this.lineHeightMult - 1) * (numLines - 1);
    width = longestLine;
    height = this.fontHeight * numLines + lineHeight;
    this.x2 = this.x1 + width;
    this.y2 = this.y1 + height;
    return { width, height };
  }

  testIfResize(pos: MousePos): { res: boolean; corner: any } {
    let points: any[] = [];
    let cornerFound: any = null;
    //top left
    points[0] = { x: this.x1, y: this.y1, corner: 0 };
    //top right
    points[1] = {
      x: this.x1 + this.width,
      y: this.y1,
      corner: 1
    };
    //bottom right
    points[3] = {
      x: this.x1 + this.width,
      y: this.y1 + this.height,
      corner: 2
    };
    //bottom left
    points[2] = {
      x: this.x1,
      y: this.y1 + this.height,
      corner: 3
    };

    let found;
    points.forEach(point => {
      if (
        Math.abs(pos.x - point.x) <= this.circleRad &&
        Math.abs(pos.y - point.y) <= this.circleRad
      ) {
        cornerFound = point.corner;
        found = true;
        return;
      }
    });
    if (found) return { res: true, corner: cornerFound };
    else return { res: false, corner: cornerFound };
  }

  calcResizeAlignment(x: number, y: number): { x: number; y: number } {
    switch (this.alignment) {
      case Alignments.left:
        this.ctx.textAlign = "start";
        return { x, y };

      case Alignments.center:
        this.ctx.textAlign = "center";
        return { x: x + this.width / 2, y };

      case Alignments.right:
        this.ctx.textAlign = "end";
        return { x: x + this.width, y };

      default:
        this.ctx.textAlign = "start";
        return { x, y };
    }
  }
  calcStaticAlignment(width: number): number {
    switch (this.alignment) {
      case Alignments.left:
        this.ctx.textAlign = "start";
        return 0;

      case Alignments.center:
        this.ctx.textAlign = "center";
        this.x1 -= (width - this.width) / 2;
        this.x2 -= (width - this.width) / 2;
        return width / 2;

      case Alignments.right:
        this.ctx.textAlign = "end";
        this.x1 -= width - this.width;
        this.x2 -= width - this.width;
        return width;

      default:
        this.ctx.textAlign = "start";
        return 0;
    }
  }

  splitText(): string[] {
    let words = [""];
    let wordCount = 0;
    let char;
    let newLineLast = false;

    for (let c = 0; c < this.text.length; c++) {
      char = this.text.charAt(c);
      if (char === " ") {
        newLineLast = false;
        words.push("");
        wordCount++;
      } else if (char === "\n") {
        if (newLineLast) {
          words[wordCount] = char;
          wordCount++;
        } else {
          words.push("\n");
          wordCount += 2;
        }
        words.push("");

        newLineLast = true;
      } else {
        newLineLast = false;
        words[wordCount] += char + this.getSpaces(this.letterSpacing);
      }
    }
    return words;
  }

  wrapText(x: number, y: number): { numLines: number; longestLine: number } {
    let line = "";
    let longestLine = 0;
    let numLines = 1;
    let words = this.splitText();
    for (let i = 0; i < words.length; i++) {
      if (words[i] === "\n") {
        //draw,test, and reset line
        line = line.trim();
        this.fontData.push({
          x,
          y: y + (numLines - 1) * this.fontHeight,
          text: line
        });
        const lineWidth = this.ctx.measureText(line).width;
        if (lineWidth > longestLine) longestLine = lineWidth;
        line = "";
        numLines++;
        y += this.fontHeight * (this.lineHeightMult - 1);
        continue;
      }
      line = line + words[i] + " ";
    }

    //draw and test last line
    line = line.trim();
    const lineWidth = this.ctx.measureText(line).width;
    if (lineWidth > longestLine) longestLine = lineWidth;
    this.fontData.push({
      x,
      y: y + (numLines - 1) * this.fontHeight,
      text: line
    });
    return { numLines, longestLine };
  }

  wrapTextBox(maxWidth: number, x: number, y: number) {
    let words = this.splitText();
    let line = "";
    let numLines = 1;

    for (let i = 0; i < words.length; i++) {
      let testLine = line + words[i];
      if (words[i] === "\n") {
        let lineHeight =
          this.fontHeight * (this.lineHeightMult - 1) * (numLines - 1);
        if (this.height < this.fontHeight * numLines + lineHeight) {
          break;
        }
        numLines++;
        this.fontData.push({ x, y, text: line.trim() });
        line = "";
        y += this.fontHeight * this.lineHeightMult;
        continue;
      }
      let metrics = this.ctx.measureText(testLine);
      let testWidth = metrics.width;
      if (testWidth > maxWidth && i !== 0) {
        let lineHeight =
          this.fontHeight * (this.lineHeightMult - 1) * (numLines - 1);
        if (this.height > this.fontHeight * numLines + lineHeight) {
          numLines++;
          this.fontData.push({ x, y, text: line.trim() });
          line = words[i] + " " + this.getSpaces(this.letterSpacing * 2);
          y += this.fontHeight * this.lineHeightMult;
        } else {
          break;
        }
      } else {
        line = testLine + " " + this.getSpaces(this.letterSpacing * 2);
      }
    }
    let lineHeight =
      this.fontHeight * (this.lineHeightMult - 1) * (numLines - 1);
    if (this.height > this.fontHeight * numLines + lineHeight) {
      this.fontData.push({ x, y, text: line.trim() });
    }
  }

  drawResizeText() {
    this.ctx.font = `${this.fontStyle} ${this.fontWeight} ${this.fontSize *
      this.ratio}px ${this.fontFamily}`;
    this.fontData.forEach(line => {
      this.ctx.fillText(
        line.text,
        line.x * this.ratio,
        (line.y + this.fontHeight) * this.ratio
      );
    });
  }

  drawStaticText(offsetX: number) {
    offsetX = offsetX || 0;
    this.ctx.font = `${this.fontStyle} ${this.fontWeight} ${this.fontSize *
      this.ratio}px ${this.fontFamily}`;
    this.fontData.forEach(line => {
      this.ctx.fillText(
        line.text,
        (this.x1 + offsetX) * this.ratio,
        (line.y + this.fontHeight) * this.ratio
      );
    });
  }

  drawCornerPoints(color: string, x: number, y: number) {
    let points: MousePos[] = [];
    //top left
    points[0] = { x, y };
    //top right
    points[1] = { x: x + this.width, y };
    //bottom left
    points[2] = { x, y: y + this.height };
    //bottom right
    points[3] = { x: x + this.width, y: y + this.height };

    points.forEach(point => {
      this.ctx.beginPath();
      this.ctx.arc(
        point.x * this.ratio,
        point.y * this.ratio,
        this.circleRad,
        0,
        2 * Math.PI,
        false
      );
      this.ctx.fillStyle = color;
      this.ctx.fill();
    });
  }

  move(pos: MousePos) {
    let moveX = pos.x - this.prevDragPos.x;
    let moveY = pos.y - this.prevDragPos.y;
    this.x1 += moveX;
    this.y1 += moveY;
    this.x2 += moveX;
    this.y2 += moveY;
    this.prevDragPos = { x: pos.x, y: pos.y };
  }

  nudge(x, y) {
    this.x1 += x;
    this.y1 += y;
    this.x2 += x;
    this.y2 += y;
  }

  callReqDraw() {
    this.drawFunc();
  }

  alignBox(dir: string) {
    const { height, width } = this.canvasHeightWidth();
    this.height = Math.abs(this.y2 - this.y1);
    this.width = Math.abs(this.x2 - this.x1);

    switch (dir) {
      case "left":
        this.x1 = this.padding.val;
        this.x2 = this.x1 + this.width;
        break;
      case "vCenter":
        this.x1 = width / 2 - this.width / 2;
        this.x2 = this.x1 + this.width;
        break;
      case "right":
        this.x2 = width - this.padding.val;
        this.x1 = this.x2 - this.width;
        break;
      case "top":
        this.y1 = this.padding.val;
        this.y2 = this.y1 + this.height;
        break;
      case "hCenter":
        this.y1 = height / 2 - this.height / 2;
        this.y2 = this.y1 + this.height;
        break;
      case "bottom":
        this.y2 = height - this.padding.val;
        this.y1 = this.y2 - this.height;
        break;

      default:
        break;
    }
    this.callReqDraw();
  }

  get data() {
    return { x: this.x1, y: this.y1, w: this.x2, g: this.y2 };
  }

  get bounds() {
    return { x1: this.x1, y1: this.y1, x2: this.x2, y2: this.y2 };
  }

  getSpaces(num: number) {
    if (num > 0) return String.fromCharCode(8202).repeat(num);
    else return "";
  }

  get fontHeight() {
    return this.fontSize;
    //return (this.baseFontHeight * this.fontSize) / 16;
  }
}
