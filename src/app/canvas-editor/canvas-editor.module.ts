import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { CanvasEditorComponent } from "./canvas-editor.component";
import { ControlsRightComponent } from "./controls/controls-right/controls-right.component";
import { CanvasComponent } from "./canvas/canvas.component";
import { ColorPickerComponent } from "./controls/controls-right/color-picker/color-picker.component";
import { VerticalSliderDirective } from "./controls/controls-right/color-picker/vertical-slider.directive";
import { ControlsLeftComponent } from "./controls/controls-left/controls-left.component";
import { ControlsBottomComponent } from "./controls/controls-bottom/controls-bottom.component";
import { ControlsTopComponent } from "./controls/controls-top/controls-top.component";
import { TooltipModule } from "ng2-tooltip-directive";

export const ROUTES: Routes = [
  {
    path: "",
    component: CanvasEditorComponent
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(ROUTES),
    TooltipModule
  ],
  declarations: [
    CanvasEditorComponent,
    ControlsRightComponent,
    CanvasComponent,
    ColorPickerComponent,
    VerticalSliderDirective,
    ControlsLeftComponent,
    ControlsBottomComponent,
    ControlsTopComponent
  ]
})
export class CanvasEditorModule {}
