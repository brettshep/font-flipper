export class CatLikes {
  fonts: number[] = [];
  selectedFont: number = null;

  select(i: number) {
    if (this.fonts.includes(i)) this.selectedFont = i;
  }
  deselect() {
    this.selectedFont = null;
  }

  add(i: number) {
    if (!this.fonts.includes(i)) this.fonts = [...this.fonts, i];
  }

  remove(i: number) {
    let index = this.fonts.indexOf(i);
    if (index > -1) {
      this.fonts.splice(index, 1);
      this.fonts = [...this.fonts];
      this.deselect();
    }
  }

  inList(i: number): boolean {
    return this.fonts.includes(i);
  }

  save(): { fonts; selectedFont } {
    return { fonts: [...this.fonts], selectedFont: this.selectedFont };
  }

  load(catLikes: { fonts; selectedFont }) {
    this.fonts = [...catLikes.fonts];
    this.selectedFont = catLikes.selectedFont;
  }
}
