import { ICatSaveGroup, IFontInfo, IFontFamily } from "../interfaces";
import { Category } from "./category/category";
import { NavigationStart, Router } from "@angular/router";
import { ICatGroup, CatID } from "../interfaces";
import { IBoxStyles } from "../interfaces";
import { Observable, Subscription } from "rxjs";
import { Component, OnInit, ViewChild, HostListener } from "@angular/core";
import { Box } from "../canvas-editor/canvas/box";
import { Store } from "../store";
import { CanvasImageComponent } from "./canvas-image/canvas-image.component";
import { first } from "rxjs/operators";
import { NavService } from "../nav.service";
import { fromEvent } from "rxjs";
import { throttleTime } from "rxjs/operators";
@Component({
  selector: "app-flip-or-flop",
  template: `
    <div class="wrapper">
      <!-- TOP-->
      <controls-top 
      class="top"
      [setBoxCat]="currCatID"
      [availCats]="catsUsed"
      [catFilter]="currCatGroup.filters.filterStats"
      (refresh)="refreshFilter($event)"
      (outBoxCat)="changeBoxCat($event)"
      ></controls-top> 

      <!-- LEFT SIDE -->
      <div class="left">
        <controls-left *ngIf="!loadingInitalFonts"
        class="leftComp sidePanel" 
        [showingLikedFont]="showingLikedFont"
        (dislike)="dislikeFont()"
        (unlike)="unlikeFont()"
        ></controls-left>
      </div>

      <!-- CENTER CANVAS -->
      <canvas-image    
        #canvasComp
        class="center"
        [image]="($imgFile | async)"
        [boxStyles]="($boxStyles | async)"
        [boxes]="($boxes | async)"
        >      
      </canvas-image>     

      <!-- RIGHT SIDE -->
      <div class="right">
        <controls-right  *ngIf="!loadingInitalFonts"
          class="rightComp sidePanel"
          [showingLikedFont]="showingLikedFont"
          [availCats]="copyCatList"
          (like)="likeFont()"
          (copyToCat)="copyToCat($event)"
        ></controls-right>
      </div>
      
      <!-- BOTTOM-->
      <controls-bottom 
        class="bottom"
        [setLikedFonts]="currCatGroup.liked.fonts"
        [setActiveFont]="currSelectedFont"
        (showLikedFont)="selectFont($event)"
        (deselectLikedFont)="deselectFont()"
        (unlikeFont)="unlikeFont($event)"
      ></controls-bottom> 

    </div>
  `,
  styleUrls: ["./flip-or-flop.component.sass"]
})
export class FlipOrFlopComponent implements OnInit {
  constructor(
    private store: Store,
    private router: Router,
    private navServ: NavService
  ) {}

  get currSelectedFont() {
    return [this.currCatGroup.liked.selectedFont];
  }
  @ViewChild(CanvasImageComponent)
  cc: CanvasImageComponent;
  $imgFile: Observable<File>;
  $boxes: Observable<Box[]>;
  $boxStyles: Observable<IBoxStyles>;
  currFont: { index: number; filter: string } = { index: null, filter: null };
  catsUsed: CatID[] = [];
  routeSub: Subscription;
  catGroup: ICatGroup = {
    0: null,
    1: null,
    2: null
  };
  currCatID: CatID;
  intialLoadSubs$: Subscription[] = [];
  catIDsToSub: CatID[] = [];
  loadingInitalFonts: boolean = true;
  copyCatList: CatID[] = [];
  eventSub: Subscription;
  ngOnInit() {
    //setup event observable
    let event$ = fromEvent(document, "keydown");
    this.eventSub = event$
      .pipe(throttleTime(200))
      .subscribe(val => this.likeOrDislikeArrows(val));
    //set image
    this.$imgFile = this.store.select<File>("image");
    //set box styles
    this.$boxStyles = this.store.select<IBoxStyles>("boxStyles");
    //set boxes
    this.$boxes = this.store.select<Box[]>("boxes");

    //clear and set boxcat
    this.catsUsed = [];
    this.store
      .select<CatID[]>("catsUsed")
      .pipe(first())
      .subscribe(val => {
        this.catsUsed = [...val];
        this.currCatID = this.catsUsed[0];
      });

    //INTIALIZE CATS
    this.store
      .select<ICatSaveGroup>("catSaveGroup")
      .pipe(first())
      .subscribe(val => {
        //loop over each box style (h1,h2,body) that was used
        this.catsUsed.forEach(catID => {
          //create new category
          this.catGroup[catID] = new Category();

          //if previous saved category
          if (val && val[catID]) {
            this.catGroup[catID].load(val[catID]);
            this.loadingInitalFonts = false;
          }
          //else init new and push inital font load subscription
          else {
            this.catGroup[catID].init();
            this.catIDsToSub.push(catID);
          }
        });
      });

    //select currfont if it exist
    this.updateCopyCatList();

    //before leaving page save data
    this.routeSub = this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        // save your data
        this.saveData();
      }
    });
  }

  ngAfterViewInit() {
    //push to subscription array
    this.catIDsToSub.forEach(catID => {
      this.intialLoadSubs$.push(
        this.catGroup[catID].fonts.fontLoader.hasLoaded$
          .pipe(first())
          .subscribe(val => {
            this.loadingInitalFonts = false;
            //pick font and show
            this.pickFont();
            this.showFont();
          })
      );
    });
    //loop and showFont
    this.catsUsed.forEach(id => {
      if (this.catGroup[id].liked.selectedFont) {
        this.showFont(id);
      }
    });

    //if previous currFont
    const index = this.currCatGroup.currFont.index;
    if (index !== null) {
      this.showFont();
    }

    //set delay before loading fonts
    setTimeout(() => {
      this.currCatGroup.fonts.refillQueues();
    }, 750);
  }

  changeBoxCat(e: CatID) {
    this.currCatID = e;
    this.currCatGroup.fonts.refillQueues();
    this.updateCopyCatList();
  }

  saveData() {
    let saveData = {};
    for (const key in this.catGroup) {
      const cat: Category = this.catGroup[key];
      if (cat) {
        saveData[key] = cat.save();
      } else {
        saveData[key] = null;
      }
    }
    this.store.set("catSaveGroup", saveData);
  }

  ngOnDestroy() {
    this.eventSub.unsubscribe();
    this.routeSub.unsubscribe();
    this.intialLoadSubs$.forEach(sub => {
      sub.unsubscribe();
    });
  }

  refreshFilter(filter: string) {
    console.log(filter);
    this.currCatGroup.fonts.refreshFilterQueue(filter);
  }

  pickFont() {
    this.currCatGroup.pickFont();
  }

  showFont(id?: CatID) {
    let family: string;
    let catID: CatID = this.currCatID;
    let group: Category = this.catGroup[catID];

    if (id || id === 0) {
      catID = id;
      group = this.catGroup[id];
    }
    const likedFont: number = group.liked.selectedFont;

    if (likedFont !== null) {
      family = FontData[likedFont].f;
    } else {
      family = FontData[group.currFont.index].f;
    }

    this.cc.DC.setBoxFont(family, "normal", "400", catID);
  }

  likeOrDislikeArrows(e) {
    if (this.showingLikedFont || this.loadingInitalFonts) return;
    //like
    if (e.keyCode === 39) {
      this.likeFont();
    }
    //dislike
    else if (e.keyCode === 37) {
      this.dislikeFont();
    }
  }

  likeFont() {
    this.currCatGroup.likeFont();
    this.pickFont();
    this.showFont();
    this.saveData();
  }

  dislikeFont() {
    // if (this.outOfFonts[this.currBoxCat]) return;
    this.currCatGroup.dislikeFont();
    this.pickFont();
    this.showFont();
  }

  selectFont(info: IFontInfo) {
    this.currCatGroup.selectFont(info.index);
    this.cc.DC.setBoxFont(info.name, "normal", "400", this.currCatID);
    this.updateCopyCatList();
  }

  deselectFont() {
    this.currCatGroup.deselectFont();
    this.showFont();
    //this.cc.DC.setBoxFont(family.f, "normal", "400", this.currCatID);
  }

  updateCopyCatList() {
    let index = this.currCatGroup.liked.selectedFont;
    if (index !== null) {
      this.copyCatList = [];
      this.catsUsed.forEach((id: CatID) => {
        if (!this.catGroup[id].liked.inList(index)) {
          this.copyCatList.push(id);
        }
      });
    }
  }

  unlikeFont(info?: IFontInfo) {
    let fontInfo = info;
    if (!fontInfo) {
      let index: number = this.currCatGroup.liked.selectedFont;
      let style: string = FontData[index].s;
      fontInfo = { index, style, name: "" };
    }
    this.currCatGroup.fonts.unlike(fontInfo);
    this.currCatGroup.liked.remove(fontInfo.index);
    this.saveData();
    this.showFont();
    //set previous
    //this.cc.DC.setBoxFont(family.f, "normal", "400", this.currCatID);
  }

  copyToCat(id: CatID) {
    let index: number = this.currCatGroup.liked.selectedFont;
    let style: string = FontData[index].s;
    this.catGroup[id].fonts.removeFont(index, style);
    this.catGroup[id].liked.add(index);
    this.updateCopyCatList();
  }

  get currCatGroup() {
    return this.catGroup[this.currCatID];
  }

  get showingLikedFont() {
    return this.currCatGroup.liked.selectedFont !== null;
  }
}
