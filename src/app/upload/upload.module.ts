import { DropFileDirective } from "./drop-file.directive";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { UploadComponent } from "./upload/upload.component";
import { RouterModule, Routes } from "@angular/router";
import { SlideshowComponent } from "./slideshow/slideshow.component";

export const ROUTES: Routes = [
  {
    path: "",
    component: UploadComponent
  }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [UploadComponent, SlideshowComponent, DropFileDirective]
})
export class UploadModule {}
