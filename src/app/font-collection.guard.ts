import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { Observable } from "rxjs";
import { Store } from "./store";
import { map } from "rxjs/operators";
import { ICatSaveGroup } from "./interfaces";

@Injectable({
  providedIn: "root"
})
export class FontCollectionGuard implements CanActivate {
  constructor(private store: Store, private router: Router) {}
  canActivate(): Observable<boolean> | Promise<boolean> | boolean {
    return this.store.select<ICatSaveGroup>("catSaveGroup").pipe(
      map(val => {
        if (val) {
          let total: number = 0;
          for (const catKey in val) {
            if (val[catKey]) {
              total += val[catKey].liked.fonts.length;
            }
          }
          if (total > 0) return true;
          else {
            this.router.navigate([`/upload`]);
            return false;
          }
        } else {
          this.router.navigate([`/upload`]);
          return false;
        }
      })
    );
  }
}
