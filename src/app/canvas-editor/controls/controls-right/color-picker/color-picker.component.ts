import { VerticalSliderDirective } from "./vertical-slider.directive";
import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  ViewChildren,
  ViewChild,
  QueryList,
  ElementRef,
  Input,
  ChangeDetectorRef
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "color-picker",
  template: `
    <div class="pickerCont" #cont>
      <div class="sliders">

        <div class="sliderCont">
          <div id="hueSlider" class="slider" VerticalSlider [min]="0" [max]="360" type="range" (value)="hueChange($event)" >
            <div class="bar"></div>
            <div class="knob"></div>
          </div>
          <div class="boxVal">
            {{hue}}
          </div>
        </div>

        <div class="sliderCont">
          <div class="slider" VerticalSlider [min]="0" [max]="100" type="range" (value)="saturationChange($event)" [ngStyle]="saturationSliderStyle">
            <div class="bar"></div>
            <div class="knob"></div>
          </div>
          <div class="boxVal">
          {{saturation}}
          </div>
        </div>

        <div class="sliderCont">
          <div class="slider" VerticalSlider [min]="0" [max]="100"type="range" (value)="valueChange($event)" [ngStyle]="valueSliderStyle">
            <div class="bar"></div>
            <div class="knob"></div>
          </div>  
          <div class="boxVal">
            {{value}}
          </div>
        </div>

        <div class="sliderCont">
          <div id="alphalider" class="slider" VerticalSlider [min]="0" [max]="100" type="range" (value)="alphaChange($event)" >
            <div class="bar"></div>
            <div class="colorBG" [ngStyle]="alphaSliderStyle"></div>
            <div class="knob"></div>
          </div>
          <div class="boxVal">
            {{alpha}}
          </div>
        </div>
      </div>

      <div class="hexColorBox">
        <span><input [(ngModel)]="currHex" maxlength="6" (keyup)="setBarsFromHex()"></span>
        <div class="colorCont">
          <div class="color"  [ngStyle]="currRGBA"></div>
        </div>
      </div>
      <div class="exit" (click)="exit()">
        <i class="fas fa-times"></i>
      </div>
    </div>
  `,
  styleUrls: ["./color-picker.component.sass"]
})
export class ColorPickerComponent {
  @Input() inputColor;
  @Output() colorsObjOut = new EventEmitter<{ rgba; hsla }>();
  @Output() close = new EventEmitter();
  @ViewChildren(VerticalSliderDirective) sliders = new QueryList();
  hue: number = 0;
  value: number = 0;
  saturation: number = 0;
  alpha: number = 0;
  currHex: string = "000000";
  listenerBind: any = this.outsideListener.bind(this);
  @ViewChild("cont") cont: ElementRef;

  constructor(private cd: ChangeDetectorRef) {}

  outsideListener(e: MouseEvent) {
    const target = e.target;
    const clickedInside = this.cont.nativeElement.contains(target);
    if (!clickedInside) {
      document.removeEventListener("mousedown", this.listenerBind);
      this.close.emit();
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      document.addEventListener("mousedown", this.listenerBind);
    }, 300);
    this.initalBarSet();
    if (!this.cd["destroyed"]) {
      this.cd.detectChanges();
    }
  }

  ngOnDestroy() {
    document.removeEventListener("mousedown", this.listenerBind);
  }

  initalBarSet() {
    const hsla = { ...this.inputColor };
    this.hue = hsla.h;
    this.saturation = hsla.s;
    this.value = hsla.l;
    this.alpha = hsla.a;
    this.setBarsInitial(hsla);
    this.setCurrHexAndOutput();
  }

  exit() {
    document.removeEventListener("mousedown", this.listenerBind);
    this.close.emit();
  }

  hueChange(val) {
    this.hue = val;
    this.setCurrHexAndOutput();
  }
  saturationChange(val) {
    this.saturation = val;
    this.setCurrHexAndOutput();
  }
  valueChange(val) {
    this.value = val;
    this.setCurrHexAndOutput();
  }
  alphaChange(val) {
    this.alpha = val;
    this.setCurrHexAndOutput();
  }

  setCurrHexAndOutput() {
    let hex = this.hslToHex(this.hue, this.saturation, this.value);
    hex = hex.substr(1);
    this.currHex = hex;
    let { r, g, b } = this.HSLToRGB(this.hue, this.saturation, this.value);
    let a = this.alpha / 100;
    this.colorsObjOut.emit({
      rgba: `rgba(${r},${g},${b},${a})`,
      hsla: { h: this.hue, s: this.saturation, l: this.value, a: this.alpha }
    });
  }

  setBarsInitial(hsla) {
    //fix h range

    hsla.h *= 100 / 360;

    this.sliders.forEach((slider: VerticalSliderDirective, index: number) => {
      switch (index) {
        case 0:
          slider.setHeight(hsla.h);
          break;
        case 1:
          slider.setHeight(hsla.s);
          break;
        case 2:
          slider.setHeight(hsla.l);
          break;
        case 3:
          slider.setHeight(hsla.a);
        default:
          break;
      }
    });
  }
  setBarsFromHex() {
    let valid = /^[0-9A-F]{6}$/i.test(this.currHex);

    if (!valid) {
      return;
    }
    const hslrgb = this.HEXtoHSL(this.currHex);
    //set local hsl
    this.hue = Math.round(hslrgb.h);
    this.saturation = Math.round(hslrgb.s);
    this.value = Math.round(hslrgb.l);

    //fix h range
    hslrgb.h *= 100 / 360;

    this.sliders.forEach((slider: VerticalSliderDirective, index: number) => {
      switch (index) {
        case 0:
          slider.setHeight(hslrgb.h);
          break;
        case 1:
          slider.setHeight(hslrgb.s);
          break;
        case 2:
          slider.setHeight(hslrgb.l);
          break;
        default:
          break;
      }
    });
    this.colorsObjOut.emit({
      rgba: `rgba(${hslrgb.r * 255},${hslrgb.g * 255},${hslrgb.b * 255},${
        this.alpha
      })`,
      hsla: { h: this.hue, s: this.saturation, l: this.value, a: this.alpha }
    });
  }

  get valueSliderStyle() {
    return {
      "background-image": `linear-gradient(white,${this.hslToHex(
        this.hue,
        this.saturation,
        50
      )}, black)`
    };
  }
  get saturationSliderStyle() {
    return {
      "background-image": `linear-gradient(${this.hslToHex(
        this.hue,
        100,
        this.value
      )}, ${this.hslToHex(0, 0, this.value)})`
    };
  }
  get alphaSliderStyle() {
    let { r, g, b } = this.HSLToRGB(this.hue, this.saturation, this.value);

    return {
      "background-image": `linear-gradient(rgba(${r},${g},${b},1),rgba(${r},${g},${b},0) `
    };
  }
  get currRGBA() {
    let { r, g, b } = this.HSLToRGB(this.hue, this.saturation, this.value);
    let a = this.alpha / 100;
    return {
      "background-color": `rgba(${r},${g},${b},${a})`
    };
  }

  HEXtoHSL(hex: string): { r; g; b; h; s; l } {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
    let r = parseInt(result[1], 16);
    let g = parseInt(result[2], 16);
    let b = parseInt(result[3], 16);
    (r /= 255), (g /= 255), (b /= 255);
    let max = Math.max(r, g, b),
      min = Math.min(r, g, b);
    let h,
      s,
      l = (max + min) / 2;
    if (max == min) {
      h = s = 0; // achromatic
    } else {
      let d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r:
          h = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g:
          h = (b - r) / d + 2;
          break;
        case b:
          h = (r - g) / d + 4;
          break;
      }
      h /= 6;
      h = h * 360;
      s = s * 100;
      l = l * 100;
    }
    return { r, g, b, h, s, l };
  }

  hslToHex(h, s, l) {
    const rgb = this.HSLToRGB(h, s, l);
    const toHex = x => {
      const hex = x.toString(16);
      return hex.length === 1 ? "0" + hex : hex;
    };
    return `#${toHex(rgb.r)}${toHex(rgb.g)}${toHex(rgb.b)}`;
  }

  HSLToRGB(h, s, l) {
    h /= 360;
    s /= 100;
    l /= 100;
    let r, g, b;
    if (s === 0) {
      r = g = b = l; // achromatic
    } else {
      const hue2rgb = (p, q, t) => {
        if (t < 0) t += 1;
        if (t > 1) t -= 1;
        if (t < 1 / 6) return p + (q - p) * 6 * t;
        if (t < 1 / 2) return q;
        if (t < 2 / 3) return p + (q - p) * (2 / 3 - t) * 6;
        return p;
      };
      const q = l < 0.5 ? l * (1 + s) : l + s - l * s;
      const p = 2 * l - q;
      r = hue2rgb(p, q, h + 1 / 3);
      g = hue2rgb(p, q, h);
      b = hue2rgb(p, q, h - 1 / 3);
    }
    r = Math.round(r * 255);
    g = Math.round(g * 255);
    b = Math.round(b * 255);

    return { r, g, b };
  }
}
