import { Store } from "./store";
import { ServiceWorkerModule } from "@angular/service-worker";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { RoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";

// angular fire
// import { AngularFireModule, FirebaseAppConfig } from "angularfire2";

// import { AngularFirestoreModule } from "angularfire2/firestore";
import { environment } from "../environments/environment";
import { NavComponent } from "./nav/nav.component";

// //tooltip
// import { TooltipModule } from "ng2-tooltip-directive";

import { SharedModule } from "./shared/shared.module";

// firebase config
// export const config: FirebaseAppConfig = {
//   apiKey: "AIzaSyAMjj8CBB2Lc1V38EIm-h2OTdDIYGnilcs",
//   authDomain: "font-flipper.firebaseapp.com",
//   databaseURL: "https://font-flipper.firebaseio.com",
//   projectId: "font-flipper",
//   storageBucket: "font-flipper.appspot.com",
//   messagingSenderId: "1018169183728"
// };

@NgModule({
  declarations: [AppComponent, NavComponent],
  imports: [
    BrowserModule,
    RoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    ServiceWorkerModule.register("/ngsw-worker.js", {
      enabled: environment.production
    }),
    SharedModule
    // TooltipModule
  ],
  providers: [Store],
  bootstrap: [AppComponent]
})
export class AppModule {}
