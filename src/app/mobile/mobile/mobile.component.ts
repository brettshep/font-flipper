import { Component } from "@angular/core";
import { trigger, transition, style, animate } from "@angular/animations";

@Component({
  selector: "mobile",
  template: `
    <div class="wrapper">
      <img src="/assets/logo.png" alt="logo" class="logo"> 
      <h1>This site isn't made for mobile, but check it out on desktop!</h1>
      <div class="buttonCont">
        <button class="blue" (click)="showSubscribe = true">
          Subscribe     
        </button>
        <a href="mailto:contact@thestudiobros.com">
          <button class="dark">
          Contact     
          </button>
        </a>
      </div>
      <div class="gifs">
        <ng-container *ngFor="let image of images">
          <div class="tuts">

            <img [src]="image.src" alt="tutorial" >
          </div>
        </ng-container>
        <div class="privacy">
          <span>
            <a routerLink="/privacy-policy">
            Privacy Policy
            </a>
          </span>
          <span>
            <a routerLink="/terms">
            Terms & Conditions
            </a>
          </span>
        </div>
      </div>
      <ng-container *ngIf="showSubscribe===true" >
        <subscribe-form 
        @fadeInOut
        (exit)="showSubscribe=false"
        ></subscribe-form>
      </ng-container>
    </div>
  `,
  styleUrls: ["./mobile.component.sass"],
  animations: [
    trigger("fadeInOut", [
      transition(":enter", [
        style({
          opacity: 0
        }),
        animate(
          ".3s ease",
          style({
            opacity: 1
          })
        )
      ]),
      transition(":leave", [
        style({
          opacity: 1
        }),
        animate(
          ".3s ease",
          style({
            opacity: 0
          })
        )
      ])
    ])
  ]
})
export class MobileComponent {
  showSubscribe: boolean = false;
  images: { src; title }[] = [
    { title: "Upload an Image", src: "/assets/GIFs/0-upload.gif" },
    { title: "Create Boxes and Enter Text", src: "/assets/GIFs/1-textbox.gif" },
    { title: "Style your Box Types", src: "/assets/GIFs/2-style.gif" },
    { title: "Filter your Fonts", src: "/assets/GIFs/3-filters.gif" },
    {
      title: "Like or Dislike the Shown Fonts",
      src: "/assets/GIFs/4-flipflop.gif"
    },
    {
      title: "View and Download your Fonts",
      src: "/assets/GIFs/5-download.gif"
    }
  ];
}
