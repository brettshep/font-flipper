import {
  Directive,
  Input,
  ElementRef,
  HostListener,
  Output,
  EventEmitter
} from "@angular/core";

@Directive({
  selector: "[VerticalSlider]"
})
export class VerticalSliderDirective {
  @Input() min;
  @Input() max;
  @Output() value = new EventEmitter<number>();
  @Output() ref = new EventEmitter<VerticalSliderDirective>();
  range: number;
  hostElem: HTMLDivElement;
  slider: HTMLDivElement;
  mouseMoveFunc;
  mouseUpFunc;

  constructor(private elemRef: ElementRef) {
    this.hostElem = elemRef.nativeElement;
  }

  ngAfterViewInit() {
    this.mouseMoveFunc = this.mouseMove.bind(this);
    this.mouseUpFunc = this.mouseUp.bind(this);
    this.slider = this.hostElem.querySelector(".bar");
    this.range = this.max - this.min;
    this.ref.emit(this);
  }

  @HostListener("mousedown", ["$event"])
  mousedown(e) {
    e.preventDefault();
    this.calcValues(e);
    //add mousemove listener
    document.addEventListener("mousemove", this.mouseMoveFunc);
    document.addEventListener("mouseup", this.mouseUpFunc);
  }

  mouseMove(e) {
    e.preventDefault();
    this.calcValues(e);
  }
  mouseUp(e) {
    e.preventDefault();
    this.calcValues(e);
    //remove listeners
    document.removeEventListener("mousemove", this.mouseMoveFunc);
    document.removeEventListener("mouseup", this.mouseUpFunc);
  }

  calcValues(e) {
    let mouseY, containerTop, newHeight, containerHeight, percentHeight, x, y;

    mouseY = e.clientY;
    containerTop = this.hostElem.getBoundingClientRect().top;
    newHeight = mouseY - containerTop;
    containerHeight = this.hostElem.offsetHeight;
    percentHeight = (newHeight * 100) / containerHeight;
    if (percentHeight <= 100 && percentHeight >= 0) {
      this.slider.style.height = percentHeight + "%";
      y = 100 - percentHeight;
      x = (y * this.range) / 100;
    } else if (percentHeight < 0) {
      percentHeight = 0;
      this.slider.style.height = percentHeight + "%";
      y = 100 - percentHeight;
      x = (y * this.range) / 100;
    } else if (percentHeight > 100) {
      percentHeight = 100;
      this.slider.style.height = percentHeight + "%";
      y = 100 - percentHeight;
      x = (y * this.range) / 100;
    }
    this.value.emit(Math.round(x));
  }

  setHeight(percentHeight) {
    this.slider.style.height = Math.abs(100 - percentHeight) + "%";
  }
}
