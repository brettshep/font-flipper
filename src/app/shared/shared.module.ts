import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { SubscribeFormComponent } from "../nav/subscribe-form/subscribe-form.component";
import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    HttpClientJsonpModule
  ],
  declarations: [SubscribeFormComponent],
  exports: [SubscribeFormComponent]
})
export class SharedModule {}
