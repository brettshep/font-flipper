import { FilterStatus, IFilterStatusGroup } from "../../interfaces";

export class CatFilters {
  filterStats: IFilterStatusGroup = {
    SANS_SERIF: FilterStatus.active,
    MONOSPACE: FilterStatus.active,
    DISPLAY: FilterStatus.active,
    SERIF: FilterStatus.active,
    HANDWRITING: FilterStatus.active
  };

  setActive(filter: string) {
    this.filterStats[filter] = FilterStatus.active;
    this.filterStats = { ...this.filterStats };
  }

  setEmpty(filter: string) {
    this.filterStats[filter] = FilterStatus.empty;
    this.filterStats = { ...this.filterStats };
  }

  save() {
    return { ...this.filterStats };
  }

  load(filterStats: IFilterStatusGroup) {
    this.filterStats = { ...filterStats };
  }

  get randomActiveFilter(): string {
    let activeFilters: string[] = [];

    for (const filter in this.filterStats) {
      if (this.filterStats[filter] === FilterStatus.active) {
        activeFilters.push(filter);
      }
    }
    if (activeFilters.length > 0) {
      return activeFilters[Math.floor(Math.random() * activeFilters.length)];
    } else return null;
  }
}
