import { Component, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "bottom-controls",
  template: `
  <div class="bottomPanel">
    <div 
      class="box" 
      [class.active]="activeBtn==='box'" 
      (click)="toggleBoxText()" tooltip="Box Text" placement="top" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
      <img src="/assets/BoxText.png" alt="box">
    </div>

    <div 
      class="box" 
      [class.active]="activeBtn==='abc'" 
      (click)="toggleABC()" tooltip="ABC's" placement="top" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
      <img src="/assets/ABCText.png" alt="abc">
    </div>
    <div
      class="box" 
      [class.active]="activeBtn==='type'" 
      (click)="toggleType()" tooltip="Custom Text" placement="top" show-delay="1000" hide-delay="0" offset="15" tooltip-class="customTooltip" theme="light">
      <img src="/assets/TypeText.png" alt="type">
    </div>
    <input
      class="textInput" 
      [class.active]="activeBtn==='type'" 
      [(ngModel)]="typeText"
      (input)="updateTypeText()"
      (focus)="focusInput($event)"
      >   

  </div>
  `,
  styleUrls: ["./bottom-controls.component.sass"]
})
export class BottomControlsComponent {
  @Output() abcOut = new EventEmitter();
  @Output() boxOut = new EventEmitter();
  @Output() typeOut = new EventEmitter<string>();
  activeBtn: string = "box";
  defaultText: string = "A quick brown fox jumps over the lazy dog.";
  typeText: string = this.defaultText;
  updateTypeText() {
    this.typeOut.emit(this.typeText);
  }

  focusInput(e) {
    if (this.typeText === this.defaultText) e.target.select();
  }

  toggleBoxText() {
    this.activeBtn = "box";
    this.boxOut.emit();
  }
  toggleABC() {
    this.activeBtn = "abc";
    this.abcOut.emit();
  }
  toggleType() {
    this.activeBtn = "type";
    this.typeOut.emit(this.typeText);
  }
}
