import { Box } from "../../canvas/box";
import { CatID } from "../../../interfaces";
import {
  Component,
  ChangeDetectionStrategy,
  Output,
  EventEmitter,
  Input
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "controls-left",
  template: `
  <figure class="panel">
    <div 
      class="styleSelector" 
      [class.active]="activeClassBox===0"
      (click)="setStyle(0)"
      >
      H1
    </div>
    <div 
      class="styleSelector" 
      [class.active]="activeClassBox===1"
      (click)="setStyle(1)">
      H2
    </div>
    <div 
      class="styleSelector" 
      [class.active]="activeClassBox===2"
      (click)="setStyle(2)">
      Body
    </div>
  </figure>
  `,
  styleUrls: ["./controls-left.component.sass"]
})
export class ControlsLeftComponent {
  activeClassBox: number = 0;
  @Input()
  set boxCat(val: CatID) {
    this.activeClassBox = val;
  }
  @Output() currStyleCat = new EventEmitter();

  setStyle(style: number) {
    this.activeClassBox = style;
    this.currStyleCat.emit(style);
  }
}
